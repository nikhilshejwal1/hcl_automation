const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProfileSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  att_id: {
    type: String,
    required: true
  },
  sap_id: {
    type: String,
    required: true
  },
  projects_assigned:{
    type: [String],
    required: true
    
  },
  emp_role:{
    type: String,
    
  },
  emp_location:{
    type: String,
    
  },

  projects: [
    {
      project_title: {
        type: String,
        required: true
      },
      project_pmt_id:{
        type:String
        
      },
      project_pid:{
        type: String,
        required: true
        
      },
      project_att_manager:{
        type: String,
        required: true
        
      },
      project_att_manager_id:{
        type: String,
        required: true
      },
      project_cr_no:{
        type: String,
        required:true
      },
      project_program:{
        type: String,
        required: true
      },
      project_sr_no:{
        type: String,
        required: true
      },
      project_project_type:{
        type: String,
        required: true
      },
      const_non_const: {
        type: String,
        default: false,
        required: true
      },
      start_date: {
        type: Date,
        
      },
      end_date: {
        type: Date,
        
      },
      m_5: {
        type: Boolean,
        default: false
      },
      m_7: {
        type: Boolean,
        default: false
      },
      m_9: {
        type: Boolean,
        default: false
      },
      warranty: {
        type: Boolean,
        default: false
      }
     
    }
  ],
  
  projemployee: [
    {
      proj_empname: {
        type: String
        
      },
      proj_emp_att_id: {
        type: String
        
      },
      proj_emp_sap_id: {
        type: String
        
      },
      proj_emprole: {
        type: String
      },
      proj_emp_bill_rate: {
        type: String
        
      },
      proj_emp_pmt_id: {
        type: String
        
      },
      proj_total_person_month:{
        type: String
      },
      sept18:{
        type: String
      },
      oct18:{
        type: String
      },
      nov18:{
        type: String
      },
      dec18:{
        type: String
      },
      jan19:{
        type: String
      },
      feb19:{
        type: String
      },
      mar19:{
        type: String
      },
      april19:{
        type: String
      },
      may19:{
        type: String
      },
      june19:{
        type: String
      },
      july19:{
        type: String
      },
      aug19:{
        type: String
      },
      sept19:{
        type: String
      }
    }
  ],

});

module.exports = Profile = mongoose.model('profile', ProfileSchema);
