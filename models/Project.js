const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProjectSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  project_title: {
    type: String,
    required: true
  },
  pmt_id: {
    type: String,
    required: true
  },
  project_pid:{
    type: String,
    required: true
    
  },
  project_att_manager:{
    type: String,
    required: true
    
  },
  project_att_manager_id:{
    type: String,
    required: true
  },
  project_cr_no:{
    type: String,
    required:true
  },
  project_program:{
    type: String,
    required: true
  },
  project_sr_no:{
    type: String,
    required: true
  },
  project_project_type:{
    type: String,
    required: true
  },
  const_non_const: {
    type: String,
    default: false,
    required: true
  },
  start_date: {
    type: String,
  },
  end_date: {
    type: String,
  },
  m_5: {
    type: String
  },
  m_7: {
    type: String
  },
  m_9: {
    type: String
  },
  warranty: {
    type: String
  },
  
  resourceloading: [
    {
      proj_empname: {
        type: String
        
      },
      proj_emp_att_id: {
        type: String
        
      },
      proj_emp_sap_id: {
        type: String
        
      },
      proj_emprole: {
        type: String
      },
      proj_emp_bill_rate: {
        type: String
        
      }
    }
  ],
});



module.exports = Project = mongoose.model('project', ProjectSchema);
