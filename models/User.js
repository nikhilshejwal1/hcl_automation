const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
  name: {
    type: String,
  },
  sap_id: {
    type: String,
    required: true
  },
  att_id: {
    type: String,
    required: true
  },
  location: {
    type: String,
  },
  password: {
    type: String,
    required: true
  },
  emp_role: {
    type: String,
    required: true
  }

});

module.exports = User = mongoose.model('users', UserSchema);
