const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EmployeeSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  emp_att_id: {
    type: String,
    required: true
  },
  emp_sap_id: {
    type: String,
    required: true
  },
  emp_name:{
    type: String,
    required: true
    
  },
  emp_location:{
    type: String,
    required: true
    
  },
  emp_role:{
    type: String,
    required: true
  },
  emp_skills:{
    type: String,
    required: true
  },
  emp_level:{
    type: String,
    required: true
    
  },
  bill_rate:{
    type: String,
    required: true
  }
  
});

module.exports = Employee = mongoose.model('employee', EmployeeSchema);
