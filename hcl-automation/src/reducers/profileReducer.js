import {
  GET_PROFILE,
  GET_PROFILES,
  GET_PROJECTS_LIST,
  PROFILE_LOADING,
  CLEAR_CURRENT_PROFILE
} from '../components/actions/types';

const initialState = {
  profile: {projects:[]},
  profiles: [],
  loading: false,
  inputValue:''
};

export default function(state = initialState, action) {
  switch (action.type) {
    case PROFILE_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_PROFILE:
      return {
        ...state,
        profile: action.payload,
        loading: false
      };
      case GET_PROJECTS_LIST:
      return {
        ...state,
        profile: action.payload,
        loading: false
      };
      case GET_PROFILES:
      return {
        ...state,
        profiles: action.payload,
        loading: false
      };
    case CLEAR_CURRENT_PROFILE:
      return {
        ...state,
        profile: null
      };
    default:
      return state;
  }
}
