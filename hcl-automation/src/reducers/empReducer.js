import {
  GET_EMPLOYEE,
  GET_EMPLOYEES,
  EMPLOYEE_LOADING,
  GET_ERRORS,
  CREATE_EMPLOYEE,
  UPDATE_EMPLOYEE,
  DELETE_EMPLOYEE
} from '../components/actions/types';

const initialState = {
  employee: null,
  employees: null,
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case EMPLOYEE_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_EMPLOYEE:
      return {
        ...state,
        employee: action.payload,
        loading: false
      };
      case GET_EMPLOYEES:
      return {
        ...state,
        employee: action.payload,
        loading: false
      };
    case GET_ERRORS:
      return {
        ...state,
        employee: null
      };
      case CREATE_EMPLOYEE:
      state.employee.push(action.payload)
      console.log("state.employee",state.employee);
      return Object.assign({}, state, state.employee);
  
      case UPDATE_EMPLOYEE:
      for(var i=0;i<state.employee.length;i++){
        if(state.employee[i]._id === action.payload._id){
          state.employee[i] = action.payload;
        }
      }
      return Object.assign({}, state, state.employee);
  
      case DELETE_EMPLOYEE:    
      console.log("state.employee is : ",state.employee);
      console.log("action.payload is : ",action.payload);
      for(var i=0;i<state.employee.length;i++){
        if(state.employee[i]._id === action.payload._id){
          state.employee.splice(i,1);
        }
      }
      console.log("state.employee is : ",state.employee);
      console.log("action.payload is : ",action.payload);
      return Object.assign({}, state, state.employee);
  
      default:
        return state;
    }
  }