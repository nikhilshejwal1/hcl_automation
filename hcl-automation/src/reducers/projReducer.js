import {
  GET_PROJECT,
  GET_PROJECTS,
  PROJECT_LOADING,
  GET_ERRORS,
  CREATE_PROJECT,
  UPDATE_PROJECT,
  DELETE_PROJECT
} from '../components/actions/types';

const initialState = {
  project: null,
  projects: null,
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case PROJECT_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_PROJECT:
      return {
        ...state,
        project: action.payload,
        loading: false
      };
      case GET_PROJECTS:
      return {
        ...state,
        project: action.payload,
        loading: false
      };
  
      
    case GET_ERRORS:
      return {
        ...state,
        project: null
      };

      case CREATE_PROJECT:
      state.project.push(action.payload)
      console.log("state.project",state.project);
      return Object.assign({}, state, state.project);
  
      case UPDATE_PROJECT:
      for(var i=0;i<state.project.length;i++){
        if(state.project[i]._id === action.payload._id){
          state.project[i] = action.payload;
        }
      }
      return Object.assign({}, state, state.project);
  
      case DELETE_PROJECT:    
      console.log("state.project is : ",state.project);
      console.log("action.payload is : ",action.payload);
      for(var i=0;i<state.project.length;i++){
        if(state.project[i]._id === action.payload._id){
          state.project.splice(i,1);
        }
      }
      console.log("state.project is : ",state.project);
      console.log("action.payload is : ",action.payload);
      return Object.assign({}, state, state.project);

    default:
      return state;
  }
}
