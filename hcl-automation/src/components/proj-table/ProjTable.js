import React, { Component } from 'react'
import classnames from 'classnames';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import {BootstrapTable, TableHeaderColumn, DeleteButton} from 'react-bootstrap-table';
import { Button, Modal,Form,Input, Icon } from 'semantic-ui-react';
import TextFieldGroup from '../common/TextFieldGroup';
import { createProject } from '../actions/projActions';
import { getProjects } from '../actions/projActions';
import SelectListGroup from '../common/SelectListGroup';
import { deleteProject } from '../actions/projActions';
import { updateProject } from '../actions/projActions';
import { ToastContainer, toast, cssTransition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Moment from 'react-moment';

Moment.globalFormat = 'YYYY MM D';
class ProjTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      project_title: '',
  pmt_id: '',
  project_pid: '',
  project_att_manager: '',
  project_att_manager_id: '',
  project_cr_no: '',
  project_sr_no: '',
  project_program: '',
  project_project_type:'',
  const_non_const:'',
  start_date:'',
  end_date:'',
  m_5:'',
  m_7:'',
  m_9:'',
  warranty:'',
  errors: {},
  modal: false,
  addmodal: false,
      redirect: false,
      editmodal: false,
      deletemodal: false,
      selected_rows: [],
      isEnabledEditBtn: false,
      isEnabledDeleteBtn: false,
      selectedAllRows: false
    };

    this.toggle = this.toggle.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onRowSelect = this.onRowSelect.bind(this);
        this.addModal = this.addModal.bind(this);
        this.editModal = this.editModal.bind(this);
        this.deleteModal = this.deleteModal.bind(this);
        this.onBeforeSaveCell = this.onBeforeSaveCell.bind(this);
        this.onAfterSaveCell = this.onAfterSaveCell.bind(this);
        this.onSelectAll = this.onSelectAll.bind(this);
  }
  componentDidMount() {
    this.props.getProjects();
   
  }
  // toggle = ()=> {
  //   this.setState({
  //     modal: !this.state.modal
  //   });
  // }
  
  addModal(){
        this.setState({
          project_title: '',
          pmt_id: '',
          project_pid: '',
          project_att_manager: '',
          project_att_manager_id: '',
          project_cr_no: '',
          project_sr_no: '',
          project_program: '',
          project_project_type:'',
          const_non_const:'',
          start_date:'',
          end_date:'',
          m_5:'',
          m_7:'',
          m_9:'',
          warranty:'',
        })
        this.setState({
          addmodal: !this.state.addmodal
        });
      }
    
      editModal(){
        this.setState({
          editmodal: !this.state.editmodal
        });
      }
    
      deleteModal = () =>{
         this.setState({
          deletemodal: !this.state.deletemodal
         });
       }
    
      toggle() {
    
        this.setState((prevState) => ({
          modal: !prevState.modal
        }));
      }
       
      onRowSelect(row, isSelected, e, rowIndex) {
        let rowStr = '';
        let rowData = [];  
        let selectedRows = this.state.selected_rows;
        console.log("current row is : ",row);
        console.log("current isSelected is : ",isSelected);
        row.isSelected = isSelected;
        if(isSelected){
          selectedRows.push(row);
        }else if(!isSelected){
          for (let i =0; i < selectedRows.length; i){
            if (selectedRows[i]._id === row._id) {
              selectedRows.splice(i,1);
              //break;
            }
          //}
        }
        }
        
        console.log("Current selectedRows is : ",selectedRows);
        if(selectedRows.length === 1){
          this.setState({  
            _id: row._id,
            project_title: row.project_title,
            pmt_id: row.pmt_id,
            project_pid: row.project_pid,
            project_att_manager: row.project_att_manager,
            project_att_manager_id: row.project_att_manager_id,
            project_cr_no: row.project_cr_no,
            project_sr_no: row.project_sr_no,
            project_program: row.project_program,
            project_project_type: row.project_project_type,
            const_non_const:row.const_non_const,
            start_date: row.start_date,
            end_date: row.end_date,
            m_5: row.m_5,
            m_7: row.m_7,
            m_9: row.m_9,
            warranty: row.warranty,
            selected_rows: selectedRows
          });
          this.setState((prevState) => ({
            isEnabledEditBtn: !prevState.isEnabledEditBtn,
            isEnabledDeleteBtn: !prevState.isEnabledDeleteBtn
          }));
          console.log("Current btn status is ",this.state.isEnabledEditBtn);
        }else if(selectedRows.length > 1) {
          this.setState({
            selected_rows: selectedRows,
            isEnabledEditBtn: false,
            isEnabledDeleteBtn: true
          });
        }else if(selectedRows.length === 0){
          this.setState({
            selected_rows: selectedRows,
            isEnabledEditBtn: false,
            isEnabledDeleteBtn: false
          });
        }
        
        // for (const prop in row) {
        //   rowStr = prop  ': "'  row[prop]  '"';
        // }
        // console.log(e);
        // console.log("in onRowSelect method - rowIndex : ", `${rowIndex}`);
        // console.log("rowStr is : ", `${rowStr}`);
        // alert(`Selected: ${isSelected}, rowIndex: ${rowIndex}, row: ${rowStr}`);
      }
    
      onSelectAll(isSelected, rows) {
        // alert(`is select all: ${isSelected}`);
        // if (isSelected) {
        //   alert('Current display and selected data: ');
        // } else {
        //   alert('unselect rows: ');
        // }
        // for (let i = 0; i < rows.length; i) {
        //   alert(rows[i].id);
        // }
        console.log("in onselectAll : isSelected ",isSelected)
        console.log("Current rows is : ",rows);
        this.setState({
          selected_rows: []
        })
    
        let selectedRows1 = this.state.selected_rows; 
        rows.forEach((row) => {
          selectedRows1.push(row); 
        });
        // selectedRows = selectedRows.push(rows); 
        this.setState({
          selected_rows: selectedRows1
        });
        if(isSelected){
          this.setState({
            selectedAllRows: true
          });
          this.setState((prevState) => ({
            isEnabledDeleteBtn: !prevState.isEnabledDeleteBtn
          }));
        }else{
          this.setState({
            selectedAllRows: false
          });
          this.setState((prevState) => ({
            isEnabledDeleteBtn: !prevState.isEnabledDeleteBtn
          }));
        }
        
      }
    
      // setRedirect = () => {
        
      //   this.setState({
      //     redirect: false
      //   })
      // }
      // renderRedirect = () => {
      //   if (this.state.redirect) {
      //     return <Redirect to='/Dashboard' />
      //   }
      // }
      
      onAfterSaveCell(row, cellName, cellValue) {
        // alert(`Save cell ${cellName} with value ${cellValue}`);
      
        // let rowStr = '';
        // for (const prop in row) {
        //   rowStr = prop  ': '  row[prop]  '\n';
        // }
      
        // alert('Thw whole row :\n'  rowStr);
        console.log("selected row is : ",row);
        let empDataUpdate = {
          _id: row._id,
          project_title: row.project_title,
          pmt_id: row.pmt_id,
          project_pid: row.project_pid,
          project_att_manager: row.project_att_manager,
          project_att_manager_id: row.project_att_manager_id,
          project_cr_no: row.project_cr_no,
          project_sr_no: row.project_sr_no,
          project_program: row.project_program,
          project_project_type: row.project_project_type,
          const_non_const:row.const_non_const,
          start_date: row.start_date,
          end_date: row.end_date,
          m_5: row.m_5,
          m_7: row.m_7,
          m_9: row.m_9,
          warranty: row.warranty,
          
        };
        // this.setState({
        //   redirect: true
        // })
        this.props.updateProject(empDataUpdate._id, empDataUpdate, this.props.history);
    
      }
    
      onBeforeSaveCell(row, cellName, cellValue) {
        // You can do any validation on here for editing value,
        // return false for reject the editing
        return true;
      }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    
    if(e.target.innerText.indexOf('Add') !== -1){
            const projData = {
              project_title: this.state.project_title,
              pmt_id: this.state.pmt_id,
              project_project_type: this.state.project_project_type,
              project_pid: this.state.project_pid,
              project_att_manager: this.state.project_att_manager,
              project_att_manager_id: this.state.project_att_manager_id,
              project_sr_no: this.state.project_sr_no,
              project_cr_no: this.state.project_cr_no,
              project_program: this.state.project_program,
              const_non_const: this.state.const_non_const,
              start_date: this.state.start_date,
              end_date: this.state.end_date,
              m_5: this.state.m_5,
              m_7: this.state.m_7,
              m_9: this.state.m_9,
              warranty: this.state.warranty,
              
            };
            this.setState({
              redirect: true
              
            })
          
            this.props.createProject(projData, this.props.history);
            this.addModal();
            toast.success("New Project Details are added Successfully !", {
              position: toast.POSITION.TOP_CENTER
            });

          }else if(e.target.innerText.indexOf('Edit') !== -1){
                  let projDataUpdate = {
                    _id : this.state._id,
                    project_title: this.state.project_title,
                    pmt_id: this.state.pmt_id,
                    project_project_type: this.state.project_project_type,
                    project_pid: this.state.project_pid,
                    project_att_manager: this.state.project_att_manager,
                    project_att_manager_id: this.state.project_att_manager_id,
                    project_sr_no: this.state.project_sr_no,
                    project_cr_no: this.state.project_cr_no,
                    project_program: this.state.project_program,
                    const_non_const: this.state.const_non_const,
                    start_date: this.state.start_date,
                    end_date: this.state.end_date,
                    m_5: this.state.m_5,
                    m_7: this.state.m_7,
                    m_9: this.state.m_9,
                    warranty: this.state.warranty,
                    
                  };
                  // this.setState({
                  //   redirect: true
                  // })
                  this.props.updateProject(projDataUpdate._id, projDataUpdate, this.props.history);
                  this.editModal();
                  toast.warn("Current Project Details updated successfully",{
                    position: toast.POSITION.TOP_CENTER
                  });
                }else if(e.target.innerText.indexOf('Delete') !== -1){
                  if(this.state.selectedAllRows || this.state.selected_rows.length > 1){
                    for (let i =0; i < this.state.selected_rows.length; i++){
                      
                    
                      this.setState({
                        redirect: true
                      })
                      this.props.deleteProject(this.state.selected_rows[i]._id,this.props.history);
                    }
                    this.deleteModal();
                    toast.error("All Selected Projects details as deleted successfully",{
                      position: toast.POSITION.TOP_CENTER
                    });
                  }else{
                    this.setState({
                      redirect: true,
                    })
                    this.props.deleteProject(this.state._id,this.props.history);
                    this.deleteModal();
                    toast.error("Selected Project details as deleted successfully",{
                      position: toast.POSITION.TOP_CENTER
                    });
                    
                  }
                }
                this.toggle();
                //window.location.href = "/dashboard";
    
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }


  createCustomToolBar = props => {
    return (
      <div style={ { padding: '1em' } }>
        { props.components.btnGroup }
        <div style={ { padding: '1em' }} className='col-lg-12'>
          { props.components.searchPanel }
        </div>
      </div>
    );
  }
  dateFormatter(cell,row) {
    return`<Moment className="start_date" format="YYYY/MM/DD"></Moment> ${cell}`
  }

  render() {
    const { errors } = this.state;
    const { project } = this.props;
    
    const projects = project.project;
      
        const selectRow = {
          mode: 'checkbox',
          columnWidth: '120px',
          showOnlySelected: true,
          clickToSelect: true,
        onSelect: this.onRowSelect,
        onSelectAll: this.onSelectAll
        };
        const options = {
          toolBar: this.createCustomToolBar,
          sortIndicator: false,
          deleteBtn: this.createCustomDeleteButton,
        };
        const cellEditProp = {
        mode: 'click',
        blurToSave: true,
        beforeSaveCell: this.onBeforeSaveCell, // a hook for before saving cell
        afterSaveCell: this.onAfterSaveCell  // a hook for after saving cell
       };
  const const_non_const = [
    { label: '* Select Construction/Non-Construction', value: 0 },
    { label: 'Construction', value: 'Construction' },
    { label: 'Non Construction', value: 'Non Construction' }
    
  ];
  const milestones = [
    { label: '* Choose Milestone Status', value: 0 },
    { label: 'Launched', value: 'Launched' },
    { label: 'Cancelled', value: 'Cancelled' },
    { label: 'Under Development', value: 'Under Development' },
    { label: 'On Hold', value: 'On Hold' }, 
  ];
  const project_program = [
    { label: '* Choose Project Program', value: 0 },
    { label: 'EDS', value: 'EDS' },
    { label: 'MDS-Checkout', value: 'MDS-Checkout' },
    { label: 'MDS-Buyflow', value: 'MDS-Buyflow' },
    { label: 'Shared Services', value: 'Shared Services' },
    { label: 'Catalog', value: 'Catalog' },
    
  ];  

  const project_project_type = [
    { label: '* Choose Project Program', value: 0 },
    { label: 'Incubator', value: 'Incubator' },
    { label: 'Variable', value: 'Variable' },
    { label: 'Fixed bid', value: 'Fixed bid' },
    { label: 'Test Only', value: 'Test Only' },
    
  ];  
    return (
      <div>
         <h2 className="display-4 float-right ">Projects List</h2>

       <Modal open={this.state.addmodal} trigger = { <Button color="secondary" style={{ margin:'.5em' }} onClick={this.addModal} size={'fullscreen'} >Add Project</Button>}closeIcon basic centered={false}>
        {/* <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}> */}
          {/* <Modal.Header toggle={this.toggle}>Add a Project</Modal.Header> */}
          <Modal.Content>
            <Form>
            <Form.Group>
              <Form.Field onSubmit={this.onSubmit}>
              <label>Project Title </label>
              <Form.Input 
              type="email" 
              name="project_title" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.project_title
              })}
              value={this.state.project_title}
              onChange={this.onChange}
              error={errors.project_title}
              placeholder="Project Title" />
               {errors.project_title && (
                    <div className="invalid-feedback">{errors.project_title}</div>
                  )}
            </Form.Field>
            <Form.Field>
            <label>Project PID </label>
              <Form.Input 
              type="name" 
              name="project_pid" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.project_pid
              })}
               value={this.state.project_pid}
              onChange={this.onChange}
              error={errors.project_pid}
              placeholder="Project PID" />
              {errors.project_pid && (
                    <div className="invalid-feedback">{errors.project_pid}</div>
                  )}
            </Form.Field>
            <Form.Field>
            <label>PMT ID </label>
              <Form.Input type="name" 
              name="pmt_id" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.pmt_id
              })}
              value={this.state.pmt_id}
              onChange={this.onChange}
              error={errors.pmt_id} 
              placeholder="Project PMT ID" />
              {errors.pmt_id && (
                    <div className="invalid-feedback">{errors.pmt_id}</div>
                  )}
            </Form.Field>
            </Form.Group>
            <Form.Group>
            <Form.Field>
            <label>ATT Manager </label>
              <Form.Input type="email"
               name="project_att_manager"
               className={classnames('form-control form-control-lg', {
                'is-invalid': errors.project_att_manager
              })}  
               value={this.state.project_att_manager}
              onChange={this.onChange}
              error={errors.project_att_manager} 
              placeholder="Project ATT Manager" />
              {errors.project_att_manager && (
                    <div className="invalid-feedback">{errors.project_att_manager}</div>
                  )}
            </Form.Field>
            <Form.Field>
            <label>ATT Manager ID </label>
              <Form.Input type="email" 
              name="project_att_manager_id"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.project_att_manager_id
              })} 
              value={this.state.project_att_manager_id}
              onChange={this.onChange}
              error={errors.project_att_manager_id} 
              placeholder="ATT Manager ID" />
              {errors.project_att_manager_id && (
                    <div className="invalid-feedback">{errors.project_att_manager_id}</div>
                  )}
            </Form.Field>
            <Form.Field>
            <label>Project CR No. </label>
              <Form.Input 
              type="email" 
              name="project_cr_no"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.project_cr_no
              })}  
              value={this.state.project_cr_no}
              onChange={this.onChange}
              error={errors.project_cr_no} 
              placeholder="Project CR No." />
              {errors.project_cr_no && (
                    <div className="invalid-feedback">{errors.project_cr_no}</div>
                  )}
            </Form.Field>
            </Form.Group>
            <Form.Group>
            <Form.Field>
            <label>Project SR No. </label>
              <Form.Input 
              type="email" 
              name="project_sr_no"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.project_sr_no
              })}
              value={this.state.project_sr_no}
              onChange={this.onChange}
              error={errors.project_sr_no}
              placeholder="Project SR No." />
              {errors.project_sr_no && (
                    <div className="invalid-feedback">{errors.project_sr_no}</div>
                  )}
            </Form.Field>

            <Form.Field>
            <label>Project Program </label>
              <SelectListGroup 
              type="email" 
              name="project_program"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.project_program
              })}
              value={this.state.project_program}
              onChange={this.onChange}
              options={project_program}
              error={errors.project_program}
              placeholder="Project Program" />
              {errors.project_program && (
                    <div className="invalid-feedback">{errors.project_program}</div>
                  )}
            </Form.Field>

            <Form.Field>
              <label>Project Type </label>
              <SelectListGroup 
              type="email" 
              name="project_project_type"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.project_project_type
              })}
              value={this.state.project_project_type}
              onChange={this.onChange}
              options={project_project_type}
              error={errors.project_project_type}
              placeholder="Project Type" />
              {errors.project_project_type && (
                    <div className="invalid-feedback">{errors.project_project_type}</div>
                  )}
            </Form.Field>
            </Form.Group>
            <Form.Group>
            <Form.Field>
            <label>Construction / Non-Construction </label>
              <SelectListGroup
              type="email" 
              name="const_non_const"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.const_non_const
              })}
              value={this.state.const_non_const}
              onChange={this.onChange}
              options={const_non_const}
              error={errors.const_non_const}
               />
              {errors.const_non_const && (
                    <div className="invalid-feedback">{errors.const_non_const}</div>
                  )}
                  </Form.Field>
                  <Form.Field>
            <label>Start Date </label>
              <TextFieldGroup
              type="date" 
              name="start_date"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.start_date
              })}
              value={this.state.start_date}
              onChange={this.onChange}
              
              error={errors.start_date}
               />
              {errors.start_date && (
                    <div className="invalid-feedback">{errors.start_date}</div>
                  )}
                  </Form.Field>

                  <Form.Field>
            <label>End Date </label>
              <TextFieldGroup
              type="date" 
              name="end_date"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.end_date
              })}
              value={this.state.end_date}
              onChange={this.onChange}
              
              error={errors.end_date}
              placeholder="Construction/Non-Construction" />
              {errors.end_date && (
                    <div className="invalid-feedback">{errors.end_date}</div>
                  )}
                  </Form.Field>


                  </Form.Group>
                     <Form.Group> 
                  <Form.Field>
                 
                  <label>M 5 Milestone </label>
                   <SelectListGroup
              type="email" 
              name="m_5"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.m_5
              })}
              value={this.state.m_5}
              onChange={this.onChange}
              options={milestones}
              error={errors.m_5}
              placeholder="M 5" />
              {errors.m_5 && (
                    <div className="invalid-feedback">{errors.m_5}</div>
                  )}
                  </Form.Field>
                  <Form.Field>
                  <label>M 7 Milestone </label>
                   <SelectListGroup
              type="email" 
              name="m_7"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.m_7
              })}
              value={this.state.m_7}
              onChange={this.onChange}
              options={milestones}
              error={errors.m_7}
              placeholder="Construction/Non-Construction" />
              {errors.m_7 && (
                    <div className="invalid-feedback">{errors.m_7}</div>
                  )}
                  </Form.Field>
                  <Form.Field>
                  <label>M 9 Milestone </label>
                   <SelectListGroup
              type="email" 
              name="m_9"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.m_9
              })}
              value={this.state.m_9}
              onChange={this.onChange}
              options={milestones}
              error={errors.m_9}
              placeholder="Construction/Non-Construction" />
              {errors.m_9 && (
                    <div className="invalid-feedback">{errors.m_9}</div>
                  )}
                  </Form.Field>
                  <Form.Field>
                  <label>Warranty </label>
                   <SelectListGroup
              type="email" 
              name="warranty"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.warranty
              })}
              value={this.state.warranty}
              onChange={this.onChange}
              options={milestones}
              error={errors.warranty}
              placeholder="Construction/Non-Construction" />
              {errors.warranty && (
                    <div className="invalid-feedback">{errors.warranty}</div>
                  )}

            </Form.Field>
            </Form.Group> 
            </Form>    
          </Modal.Content>
          <Modal.Actions>
             <Button color="green" 
             inverted
             type="submit"
             value="Submit"
             className={classnames('primary', {
               'disabled': errors.project_pid
             })}
             onClick={this.onSubmit}> 
             <Icon name='checkmark' />
             Add Project</Button>{' '}
             

            <Button color="red" inverted onClick={this.addModal}> 
             <Icon name='remove' />Exit</Button>
           </Modal.Actions>
        </Modal>

  {/* For Edit poject */}

   <Modal open={this.state.editmodal} trigger = { <Button color="secondary" style={{ margin:'.5em' }} disabled={!this.state.isEnabledEditBtn} onClick={this.editModal} size={'fullscreen'} >Edit Project</Button>}closeIcon basic centered={false}>
         {/* <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}> */}
           {/* <Modal.Header toggle={this.toggle}>Add a Project</Modal.Header> */}
           <Modal.Content>
             <Form>
             <Form.Group>
               <Form.Field onSubmit={this.onSubmit}>
               <label>Project Title </label>
               <Form.Input 
               type="email" 
               name="project_title" 
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.project_title
               })}
               value={this.state.project_title}
               onChange={this.onChange}
               error={errors.project_title}
               placeholder="Project Title" />
                {errors.project_title && (
                     <div className="invalid-feedback">{errors.project_title}</div>
                   )}
             </Form.Field>
             <Form.Field>
             <label>Project PID </label>
               <Form.Input 
               type="name" 
               name="project_pid" 
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.project_pid
               })}
                value={this.state.project_pid}
               onChange={this.onChange}
               error={errors.project_pid}
               placeholder="Project PID" />
               {errors.project_pid && (
                     <div className="invalid-feedback">{errors.project_pid}</div>
                   )}
             </Form.Field>
             <Form.Field>
             <label>PMT ID </label>
               <Form.Input type="name" 
               name="pmt_id" 
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.pmt_id
               })}
               value={this.state.pmt_id}
               onChange={this.onChange}
               error={errors.pmt_id} 
               placeholder="Project PMT ID" />
               {errors.pmt_id && (
                     <div className="invalid-feedback">{errors.pmt_id}</div>
                   )}
             </Form.Field>
             </Form.Group>
             <Form.Group>
             <Form.Field>
             <label>ATT Manager </label>
               <Form.Input type="email"
                name="project_att_manager"
                className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.project_att_manager
               })}  
                value={this.state.project_att_manager}
               onChange={this.onChange}
               error={errors.project_att_manager} 
               placeholder="Project ATT Manager" />
               {errors.project_att_manager && (
                     <div className="invalid-feedback">{errors.project_att_manager}</div>
                   )}
             </Form.Field>
             <Form.Field>
             <label>ATT Manager ID </label>
               <Form.Input type="email" 
               name="project_att_manager_id"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.project_att_manager_id
               })} 
               value={this.state.project_att_manager_id}
               onChange={this.onChange}
               error={errors.project_att_manager_id} 
               placeholder="ATT Manager ID" />
               {errors.project_att_manager_id && (
                     <div className="invalid-feedback">{errors.project_att_manager_id}</div>
                   )}
             </Form.Field>
             <Form.Field>
             <label>Project CR No. </label>
               <Form.Input 
               type="email" 
               name="project_cr_no"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.project_cr_no
               })}  
               value={this.state.project_cr_no}
               onChange={this.onChange}
               error={errors.project_cr_no} 
               placeholder="Project CR No." />
               {errors.project_cr_no && (
                     <div className="invalid-feedback">{errors.project_cr_no}</div>
                   )}
             </Form.Field>
             </Form.Group>
             <Form.Group>
             <Form.Field>
             <label>Project SR No. </label>
               <Form.Input 
               type="email" 
               name="project_sr_no"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.project_sr_no
               })}
               value={this.state.project_sr_no}
               onChange={this.onChange}
               error={errors.project_sr_no}
               placeholder="Project SR No." />
               {errors.project_sr_no && (
                     <div className="invalid-feedback">{errors.project_sr_no}</div>
                   )}
             </Form.Field>
 
             <Form.Field>
             <label>Project Program </label>
               <SelectListGroup 
               type="email" 
               name="project_program"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.project_program
               })}
               value={this.state.project_program}
               onChange={this.onChange}
               options={project_program}
               error={errors.project_program}
               placeholder="Project Program" />
               {errors.project_program && (
                     <div className="invalid-feedback">{errors.project_program}</div>
                   )}
             </Form.Field>
 
             <Form.Field>
               <label>Project Type </label>
               <SelectListGroup 
               type="email" 
               name="project_project_type"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.project_project_type
               })}
               value={this.state.project_project_type}
               onChange={this.onChange}
               options={project_project_type}
               error={errors.project_project_type}
               placeholder="Project Type" />
               {errors.project_project_type && (
                     <div className="invalid-feedback">{errors.project_project_type}</div>
                   )}
             </Form.Field>
             </Form.Group>
             <Form.Group>
             <Form.Field>
             <label>Construction / Non-Construction </label>
               <SelectListGroup
               type="email" 
               name="const_non_const"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.const_non_const
               })}
               value={this.state.const_non_const}
               onChange={this.onChange}
               options={const_non_const}
               error={errors.const_non_const}
                />
               {errors.const_non_const && (
                     <div className="invalid-feedback">{errors.const_non_const}</div>
                   )}
                   </Form.Field>
                   <Form.Field>
             <label>Start Date </label>
               <TextFieldGroup
               type="date" 
               name="start_date"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.start_date
               })}
               value={this.state.start_date}
               onChange={this.onChange}
               
               error={errors.start_date}
                />
               {errors.start_date && (
                     <div className="invalid-feedback">{errors.start_date}</div>
                   )}
                   </Form.Field>
 
                   <Form.Field>
             <label>End Date </label>
               <TextFieldGroup
               type="date" 
               name="end_date"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.end_date
               })}
               value={this.state.end_date}
               onChange={this.onChange}
               
               error={errors.end_date}
               placeholder="Construction/Non-Construction" />
               {errors.end_date && (
                     <div className="invalid-feedback">{errors.end_date}</div>
                   )}
                   </Form.Field>
 
 
                   </Form.Group>
                      <Form.Group> 
                   <Form.Field>
                  
                   <label>M 5 Milestone </label>
                    <SelectListGroup
               type="email" 
               name="m_5"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.m_5
               })}
               value={this.state.m_5}
               onChange={this.onChange}
               options={milestones}
               error={errors.m_5}
               placeholder="M 5" />
               {errors.m_5 && (
                     <div className="invalid-feedback">{errors.m_5}</div>
                   )}
                   </Form.Field>
                   <Form.Field>
                   <label>M 7 Milestone </label>
                    <SelectListGroup
               type="email" 
               name="m_7"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.m_7
               })}
               value={this.state.m_7}
               onChange={this.onChange}
               options={milestones}
               error={errors.m_7}
               placeholder="Construction/Non-Construction" />
               {errors.m_7 && (
                     <div className="invalid-feedback">{errors.m_7}</div>
                   )}
                   </Form.Field>
                   <Form.Field>
                   <label>M 9 Milestone </label>
                    <SelectListGroup
               type="email" 
               name="m_9"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.m_9
               })}
               value={this.state.m_9}
               onChange={this.onChange}
               options={milestones}
               error={errors.m_9}
               placeholder="Construction/Non-Construction" />
               {errors.m_9 && (
                     <div className="invalid-feedback">{errors.m_9}</div>
                   )}
                   </Form.Field>
                   <Form.Field>
                   <label>Warranty </label>
                    <SelectListGroup
               type="email" 
               name="warranty"
               className={classnames('form-control form-control-lg', {
                 'is-invalid': errors.warranty
               })}
               value={this.state.warranty}
               onChange={this.onChange}
               options={milestones}
               error={errors.warranty}
               placeholder="Construction/Non-Construction" />
               {errors.warranty && (
                     <div className="invalid-feedback">{errors.warranty}</div>
                   )}
 
             </Form.Field>
             </Form.Group> 
             </Form>    
           </Modal.Content>
           <Modal.Actions>
             <Button color="green" 
             inverted
             type="submit"
             value="Submit"
             className={classnames('primary', {
               'disabled': errors.project_pid
             })}
             onClick={this.onSubmit}> 
             <Icon name='checkmark' />
             Edit Project</Button>{' '}
             
            <Button color="red" inverted onClick={this.editModal}> 
             <Icon name='remove' />Exit</Button>
           </Modal.Actions>
         </Modal>
 
         {/* For Delete Project */}
 

       <Modal open={this.state.deletemodal} trigger = { <Button color="secondary" style={{ margin:'.5em' }} disabled={!this.state.isEnabledDeleteBtn} onClick={this.deleteModal} size={'fullscreen'} >Delete Project</Button>}closeIcon basic centered={false}>
         {/* <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}> */}
           {/* <Modal.Header toggle={this.toggle}>Add a Project</Modal.Header> */}
           <Modal.Content>
              
          <Form onSubmit={this.onSubmit}>
                Are you sure you want delete selected project detail?
            </Form>
           </Modal.Content>
           <Modal.Actions>
             <Button color="green" 
             inverted
             type="submit"
             value="Submit"
             className={classnames('primary', {
               'disabled': errors.project_pid
             })}
             onClick={this.onSubmit}> 
             <Icon name='checkmark' />
             Delete Project</Button>{' '}
             
         
            <Button color="red" inverted onClick={this.deleteModal}> 
             <Icon name='remove' />Exit</Button>
           </Modal.Actions>

         </Modal>

      <BootstrapTable data={ projects }
        options={ options }
        selectRow={ selectRow }
        headerStyle={ { background: 'rgba(51,51,51,0.425)', color: 'white' }}
        tableStyle={ { background: 'rgba(51,51,51,0.425)', margin:'.5em' } }
        bodyStyle={ { background: 'rgba(51,51,51,0.425)', margin:'.1em' } }
        striped
        pagination
        deleteRow
        exportCSV
        csvFileName='Projects-List.csv'
        search
        cellEdit={ cellEditProp }
        >
        
        <TableHeaderColumn 
        dataField='project_title' 
        isKey={ true }
        width={120}>
         Title
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='pmt_id'
        width={120}>
         PMT ID
         </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='project_pid'
        width={120}>
         Project PID
         </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='project_att_manager'
        width={120}>
          Manager
         </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='project_att_manager_id'
        width={120}>
          Manager ID
         </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='project_cr_no'
        width={120}>
          CR No.
         </TableHeaderColumn>

       
        <TableHeaderColumn 
        dataField='project_sr_no' 
        width={120}> SR No.
        </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='project_program' 
        width={120}> Program
        </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='project_project_type' 
        width={120}> Type
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='start_date' 
        width={120}
        dataFormat={ this.dateFormatter }
        > Start Date
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='end_date' 
        width={120}
        dataFormat={ this.dateFormatter }
        > End Date
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='m_5' 
        width={120}
        > M5
        </TableHeaderColumn> 

        <TableHeaderColumn 
        dataField='m_7'
        width={120} 
        > M7
        </TableHeaderColumn>  

        <TableHeaderColumn 
        dataField='m_9' 
        width={120}
        > M9
        </TableHeaderColumn>  

        <TableHeaderColumn 
        dataField='warranty' 
        width={120}
        > Warranty
        </TableHeaderColumn>     
      </BootstrapTable>
      

      </div>
    )
  }
}


ProjTable.propTypes = {
  project: PropTypes.object.isRequired,
  getProjects: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  project: state.project,
  errors: state.errors
});

export default connect(mapStateToProps, { createProject,updateProject,deleteProject,getProjects })(
  withRouter(ProjTable)
);
