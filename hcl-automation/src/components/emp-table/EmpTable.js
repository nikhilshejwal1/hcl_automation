import React, { Component } from 'react'
import classnames from 'classnames';
import { connect } from 'react-redux';
import ReactDOM, {render} from 'react-dom';
import { withRouter, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import {BootstrapTable, TableHeaderColumn, DeleteButton} from 'react-bootstrap-table';
import { Button, Modal,Form,Input,Icon,Message, Header } from 'semantic-ui-react';
import { createEmployee } from '../actions/empActions';
import { getEmployees } from '../actions/empActions';
import TextFieldGroup from '../common/TextFieldGroup';
import SelectListGroup from '../common/SelectListGroup';
import { deleteEmployee } from '../actions/empActions';
import { updateEmployee } from '../actions/empActions';
import { ToastContainer, toast, cssTransition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Slide, Zoom, Flip, Bounce } from 'react-toastify';

class EmpTable extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
     
      emp_sap_id: '',
      emp_name: '',
      bill_rate: '',
      emp_level: '',
      emp_att_id: '',
      emp_role: '',
      emp_skills: '',
      emp_location: '',
      projects_assigned: '',
      errors: {},
      modal: false,
      addmodal: false,
      redirect: false,
      editmodal: false,
      deletemodal: false,
      selected_rows: [],
      isEnabledEditBtn: false,
      isEnabledDeleteBtn: false,
      selectedAllRows: false
    };

    this.toggle = this.toggle.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onRowSelect = this.onRowSelect.bind(this);
    this.addModal = this.addModal.bind(this);
    this.editModal = this.editModal.bind(this);
    this.deleteModal = this.deleteModal.bind(this);
    this.onBeforeSaveCell = this.onBeforeSaveCell.bind(this);
    this.onAfterSaveCell = this.onAfterSaveCell.bind(this);
    this.onSelectAll = this.onSelectAll.bind(this);
  }
  componentDidMount() {
    this.props.getEmployees();
  }
  addModal(){
    this.setState({
      emp_sap_id: '',
      emp_name: '',
      bill_rate: '',
      emp_level: '',
      emp_att_id: '',
      emp_role: '',
      emp_skills: '',
      emp_location: '',
      projects_assigned: ''
    })
    this.setState({
      addmodal: !this.state.addmodal
    });
  }

  editModal(){
    this.setState({
      editmodal: !this.state.editmodal
    });
  }

  deleteModal = () =>{
    this.setState({
      deletemodal: !this.state.deletemodal
    });
  }

  toggle() {

    this.setState((prevState) => ({
      modal: !prevState.modal
    }));

   /*  console.log("this.state.modal is : ",this.state.modal);
    if(this.state.modal === false){
      this.setState({
        modal: !this.state.modal
      });
    }else{
      this.setState({
        emp_sap_id: '',
        emp_name: '',
        bill_rate: '',
        emp_level: '',
        emp_att_id: '',
        emp_role: '',
        emp_skills: '',
        emp_location: '',
        projects_assigned: '',
        modal: !this.state.modal
      });
    } */
  }
  onRowSelect(row, isSelected, e, rowIndex) {
    let rowStr = '';
    let rowData = [];  
    let selectedRows = this.state.selected_rows;
    console.log("current row is : ",row);
    console.log("current isSelected is : ",isSelected);
    row.isSelected = isSelected;
    if(isSelected){
      selectedRows.push(row);
    }else if(!isSelected){
      for (let i =0; i < selectedRows.length; i++){
        if (selectedRows[i]._id === row._id) {
          selectedRows.splice(i,1);
          //break;
        }
      //}
    }
    }
    
    console.log("Current selectedRows is : ",selectedRows);
    if(selectedRows.length === 1){
      this.setState({  
        _id: row._id,   
        emp_sap_id: row.emp_sap_id,
        emp_name: row.emp_name,
        bill_rate: row.bill_rate,
        emp_level: row.emp_level,
        emp_att_id: row.emp_att_id,
        emp_role: row.emp_role,
        emp_skills: row.emp_skills,
        emp_location: row.emp_location,
        projects_assigned: '',
        selected_rows: selectedRows
      });
      this.setState((prevState) => ({
        isEnabledEditBtn: !prevState.isEnabledEditBtn,
        isEnabledDeleteBtn: !prevState.isEnabledDeleteBtn
      }));
      console.log("Current btn status is ",this.state.isEnabledEditBtn);
    }else if(selectedRows.length > 1) {
      this.setState({
        selected_rows: selectedRows,
        isEnabledEditBtn: false,
        isEnabledDeleteBtn: true
      });
    }else if(selectedRows.length === 0){
      this.setState({
        selected_rows: selectedRows,
        isEnabledEditBtn: false,
        isEnabledDeleteBtn: false
      });
    }
    
    // for (const prop in row) {
    //   rowStr += prop + ': "' + row[prop] + '"';
    // }
    // console.log(e);
    // console.log("in onRowSelect method - rowIndex : ", `${rowIndex}`);
    // console.log("rowStr is : ", `${rowStr}`);
    // alert(`Selected: ${isSelected}, rowIndex: ${rowIndex}, row: ${rowStr}`);
  }

  onSelectAll(isSelected, rows) {
    // alert(`is select all: ${isSelected}`);
    // if (isSelected) {
    //   alert('Current display and selected data: ');
    // } else {
    //   alert('unselect rows: ');
    // }
    // for (let i = 0; i < rows.length; i++) {
    //   alert(rows[i].id);
    // }
    console.log("in onselectAll : isSelected ",isSelected)
    console.log("Current rows is : ",rows);
    this.setState({
      selected_rows: []
    })

    let selectedRows1 = this.state.selected_rows; 
    rows.forEach((row) => {
      selectedRows1.push(row); 
    });
    // selectedRows = selectedRows.push(rows); 
    this.setState({
      selected_rows: selectedRows1
    });
    if(isSelected){
      this.setState({
        selectedAllRows: true
      });
      this.setState((prevState) => ({
        isEnabledDeleteBtn: !prevState.isEnabledDeleteBtn
      }));
    }else{
      this.setState({
        selectedAllRows: false
      });
      this.setState((prevState) => ({
        isEnabledDeleteBtn: !prevState.isEnabledDeleteBtn
      }));
    }
    
  }

  // setRedirect = () => {
    
  //   this.setState({
  //     redirect: false
  //   })
  // }
  // renderRedirect = () => {
  //   if (this.state.redirect) {
  //     return <Redirect to='/Dashboard' />
  //   }
  // }
  
  onAfterSaveCell(row, cellName, cellValue) {
    // alert(`Save cell ${cellName} with value ${cellValue}`);
  
    // let rowStr = '';
    // for (const prop in row) {
    //   rowStr += prop + ': ' + row[prop] + '\n';
    // }
  
    // alert('Thw whole row :\n' + rowStr);

    let empDataUpdate = {
      _id: row._id,
      emp_name: row.emp_name,
      emp_sap_id: row.emp_sap_id,
      emp_att_id: row.emp_att_id,
      bill_rate: row.bill_rate,
      emp_role: row.emp_role,
      emp_skills:row.emp_skills,
      emp_level: row.emp_level,
      emp_location: row.emp_location,
      
    };
    // this.setState({
    //   redirect: true
    // })
    this.props.updateEmployee(empDataUpdate._id, empDataUpdate, this.props.history);

  }

  onBeforeSaveCell(row, cellName, cellValue) {
    // You can do any validation on here for editing value,
    // return false for reject the editing
    return true;
  }
  

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    if(e.target.innerText.indexOf('Add') !== -1){
      const empData = {
        emp_name: this.state.emp_name,
        emp_sap_id: this.state.emp_sap_id,
        emp_att_id: this.state.emp_att_id,
        bill_rate: this.state.bill_rate,
        emp_role: this.state.emp_role,
        emp_skills:this.state.emp_skills,
        emp_level: this.state.emp_level,
        emp_location: this.state.emp_location,
        
      };
      this.setState({
        redirect: true
        
      })
    
      this.props.createEmployee(empData, this.props.history);
      this.addModal();
      toast.success("New Employee Details are added Successfully !", {
        position: toast.POSITION.TOP_CENTER
      });

    }else if(e.target.innerText.indexOf('Edit') !== -1){
      let empDataUpdate = {
        _id: this.state._id,
        emp_name: this.state.emp_name,
        emp_sap_id: this.state.emp_sap_id,
        emp_att_id: this.state.emp_att_id,
        bill_rate: this.state.bill_rate,
        emp_role: this.state.emp_role,
        emp_skills:this.state.emp_skills,
        emp_level: this.state.emp_level,
        emp_location: this.state.emp_location,
        
      };
      this.setState({
        redirect: true
      })
      this.props.updateEmployee(this.state._id, empDataUpdate, this.props.history);
      this.editModal();
      toast.warn("Current Employee Details updated successfully",{
        position: toast.POSITION.TOP_CENTER
      });
    }else if(e.target.innerText.indexOf('Delete') !== -1){
      if(this.state.selectedAllRows || this.state.selected_rows.length > 1){
        for (let i =0; i < this.state.selected_rows.length; i++){
          
        
          this.setState({
            redirect: true
          })
          this.props.deleteEmployee(this.state.selected_rows[i]._id,this.props.history);
        }
        this.deleteModal();
        toast.error("All Selected Employees details as deleted successfully",{
          position: toast.POSITION.TOP_CENTER
        });
      }else{
        this.setState({
          redirect: true,
        })
        this.props.deleteEmployee(this.state._id,this.props.history);
        this.deleteModal();
        toast.error("Selected Employee details as deleted successfully",{
          position: toast.POSITION.TOP_CENTER
        });
        
      }
    }
    this.toggle();
    //window.location.href = "/dashboard";
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  createCustomToolBar = props => {
    return (
      <div style={ { padding: '1em' } }>
        { props.components.btnGroup }
        <div style={ { padding: '1em' }} className='col-lg-12'>
          { props.components.searchPanel }
        </div>
      </div>
    );
  }

  handleDeleteButtonClick = (onClick) => {
    // Custom your onClick event here,
    // it's not necessary to implement this function if you have no any process before onClick
    console.log('This is my custom function for DeleteButton click event');
    onClick();

  }

  createCustomDeleteButton = (onClick,onRowSelect) => {
    return (
      <DeleteButton
        btnText='CustomDeleteText'
        btnContextual='btn-success'
        className='my-custom-class'
        btnGlyphicon='glyphicon-edit'
        onClick={ e => this.handleDeleteButtonClick(onClick) }/>
    );
    // If you want have more power to custom the child of DeleteButton,
    // you can do it like following
    // return (
    //   <DeleteButton
    //     btnContextual='btn-warning'
    //     className='my-custom-class'
    //     onClick={ () => this.handleDeleteButtonClick(onClick) }>
    //     { ... }
    //   </DeleteButton>
    // );
  }


  render() {
    const { errors } = this.state;
    const { employee } = this.props;
    const employees = employee.employee;
    // const { selected_rows } = this.state
    // const isEnabled =
    // selected_rows.length == 1;
    // console.log("selected_rows.length == 1 is : ",selected_rows.length == 1);
  const selectRow = {
    mode: 'checkbox',
    columnWidth: '120px',
    showOnlySelected: true,
    clickToSelect: true,
  onSelect: this.onRowSelect,
  onSelectAll: this.onSelectAll
  };
  const options = {
    toolBar: this.createCustomToolBar,
    sortIndicator: false,
    deleteBtn: this.createCustomDeleteButton,
  };
  const cellEditProp = {
  mode: 'click',
  blurToSave: true,
  beforeSaveCell: this.onBeforeSaveCell, // a hook for before saving cell
  afterSaveCell: this.onAfterSaveCell  // a hook for after saving cell
};
  // function onRowSelect(row, isSelected, e, rowIndex) {
  //   let rowStr = '';
  //   let rowData = [];
  //   console.log(" row value is ",row);
  //   console.log("row id is : ",row._id);
  //   this.setState({     
  //     emp_sap_id: row.emp_sap_id,
  //     emp_name: row.emp_name,
  //     bill_rate: row.bill_rate,
  //     emp_level: row.emp_level,
  //     emp_att_id: row.emp_att_id,
  //     emp_role: row.emp_role,
  //     emp_skills: row.emp_skills,
  //     emp_location: row.emp_location,
  //     projects_assigned: ''
  //   });
  //   for (const prop in row) {
  //     rowStr += prop + ': "' + row[prop] + '"';
  //   }
  //   console.log(e);
  //   console.log("in onRowSelect method - rowIndex : ", `${rowIndex}`);
  //   console.log("rowStr is : ", `${rowStr}`);
  //   alert(`Selected: ${isSelected}, rowIndex: ${rowIndex}, row: ${rowStr}`);
  // }
  
  // function onSelectAll(isSelected, rows) {
  //   console.log("All Rows data : ",rows);
  //   alert(`is select all: ${isSelected}`);
  //   if (isSelected) {
  //     alert('Current display and selected data: ');
  //   } else {
  //     alert('unselect rows: ');
  //   }
  //   for (let i = 0; i < rows.length; i++) {
  //     alert(rows[i].id);
  //   }
  // }

  const skills = [
    { label: '* Select Employee Skills', value: 0 },
    { label: 'Front End', value: 'Front End' },
    { label: 'Back End', value: 'Back End' },
    { label: 'QA', value: 'QA' },
    { label: 'Scrum Master', value: 'Scrum Master' }
    
  ];

  const emp_role = [
    { label: '* Select Employee Role', value: 0 },
    { label: 'Jr.', value: 'Jr.' },
    { label: 'Sr.', value: 'Sr.' },
    { label: 'MID', value: 'MID' },
    { label: 'SME', value: 'SME' }

  ];

  const emp_location= [
    { label: '* Select Employee Location', value: 0 },
    { label: 'On-shore', value: 'On-shore' },
    { label: 'Off-shore', value: 'Off-shore.' },
  ];

  const emp_level = [
    { label: '* Select Employee Level', value: 0 },
    { label: 'E0', value: 'E0' },
    { label: 'E1', value: 'E1' },
    { label: 'E2', value: 'E2' },
    { label: 'E3', value: 'E3' },
    { label: 'E4', value: 'E4' },
    { label: 'E5', value: 'E5' },
    { label: 'E6', value: 'E6' },
    { label: 'E7', value: 'E7' },
    { label: 'E8', value: 'E8' },
  ];

    return (
      <div>
      <br/>
         <h2 className="display-4 float-right ">Employees List</h2>
       <Modal open={this.state.addmodal} trigger={<Button size='small' color="olive" style={{ margin:'.5em' }} onClick={this.addModal}><Icon name='add user'></Icon>Add Employee</Button>} closeIcon basic centered={false}>
        
          <Modal.Header toggle={this.toggle}>Add an Employee</Modal.Header>
          <Modal.Content>

              <Form onSubmit={this.onSubmit}>
                <Form.Group >
              {/* <FormGroup onSubmit={this.onSubmit}> */}
              <Form.Field>
              
              <Form.Input 
              type="email" 
              name="emp_name" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_name
              })}
              value={this.state.emp_name}
              onChange={this.onChange}
              error={errors.emp_name}
              placeholder="Employee Name" />
               {errors.emp_name && (
                    <div className="invalid-feedback">{errors.emp_name}</div>
                  )}
            </Form.Field>
            <Form.Field>
    
              <Form.Input 
              type="name" 
              name="emp_att_id" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_att_id
              })}
               value={this.state.emp_att_id}
              onChange={this.onChange}
              error={errors.emp_att_id}
              placeholder="Employee ATT ID" />
              {errors.emp_att_id && (
                    <div className="invalid-feedback">{errors.emp_att_id}</div>
                  )}
            </Form.Field>
            <Form.Field>
    
              <Form.Input type="name" 
              name="emp_sap_id" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_sap_id
              })}
              value={this.state.emp_sap_id}
              onChange={this.onChange}
              error={errors.emp_sap_id} 
              placeholder="Employee SAP ID" />
              {errors.emp_sap_id && (
                    <div className="invalid-feedback">{errors.emp_sap_id}</div>
                  )}
            </Form.Field>
            </Form.Group>
            <Form.Group >
            <Form.Field>
        
              <SelectListGroup type="email"
               name="emp_location"
               className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_location
              })}  
               value={this.state.emp_location}
              onChange={this.onChange}
              options={emp_location}
              error={errors.emp_location} 
              placeholder="Employee Location" />
              {errors.emp_location && (
                    <div className="invalid-feedback">{errors.emp_location}</div>
                  )}
            </Form.Field>
            <Form.Field>
          
              <Form.Input type="email" 
              name="bill_rate"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.bill_rate
              })} 
              value={this.state.bill_rate}
              onChange={this.onChange}
              error={errors.bill_rate} 
              placeholder="Employee Bill Rate" />
              {errors.bill_rate && (
                    <div className="invalid-feedback">{errors.bill_rate}</div>
                  )}
            </Form.Field>
            <Form.Field>
             
              <SelectListGroup 
              type="email" 
              name="emp_level"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_level
              })}  
              value={this.state.emp_level}
              onChange={this.onChange}
              options={emp_level}
              error={errors.emp_level} 
              placeholder="Employee Level" />
              {errors.emp_level && (
                    <div className="invalid-feedback">{errors.emp_level}</div>
                  )}
            </Form.Field>
            </Form.Group>
            <Form.Group >
            <Form.Field>
              
              <SelectListGroup 
              type="email" 
              name="emp_role"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_role
              })}
              value={this.state.emp_role}
              onChange={this.onChange}
              options={emp_role}
              error={errors.emp_role}
              placeholder="Employee Role" />
              {errors.emp_role && (
                    <div className="invalid-feedback">{errors.emp_role}</div>
                  )}
            </Form.Field>
            <Form.Field>
              
              <SelectListGroup 
              type="email" 
              name="emp_skills"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_skills
              })}
              value={this.state.emp_skills}
              onChange={this.onChange}
              error={errors.emp_skills}
              options={skills}
              placeholder="Employee Skills" />
              {errors.emp_skills && (
                    <div className="invalid-feedback">{errors.emp_skills}</div>
                  )}
                  
            </Form.Field>
            </Form.Group>
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button color="olive" 
            type="submit"
            
            value="Submit"
            disabled={!(this.state.emp_sap_id && this.state.emp_name && this.state.bill_rate && this.state.emp_level && this.state.emp_att_id && this.state.emp_role && this.state.emp_skills && this.state.emp_location)}
            onClick={this.onSubmit}><Icon name='add user'></Icon>Add Employee</Button>{' '}
            <Button color="secondary" onClick={this.addModal}>Exit</Button>
          </Modal.Actions>
        </Modal>
        <Modal open={this.state.editmodal} trigger={<Button size='small' color="yellow" style={{ margin:'.5em' }} disabled={!this.state.isEnabledEditBtn} onClick={this.editModal}><Icon name='edit'></Icon>Edit Employee</Button>} closeIcon basic centered={false}>
        
          <Modal.Header toggle={this.toggle}><Icon name='edit'></Icon>Edit Employee </Modal.Header>
          <Message floating size='tiny' icon='info circle' color='yellow' header='Multiple employee information can be edited by clicking on their details in the table'/>
          <Modal.Content>

              <Form onSubmit={this.onSubmit}>
                <Form.Group >
              {/* <FormGroup onSubmit={this.onSubmit}> */}
              <Form.Field>
              
              <Form.Input 
              type="email" 
              name="emp_name" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_name
              })}
              value={this.state.emp_name}
              onChange={this.onChange}
              error={errors.emp_name}
              placeholder="Employee Name" />
               {errors.emp_name && (
                    <div className="invalid-feedback">{errors.emp_name}</div>
                  )}
            </Form.Field>
            <Form.Field>
    
              <Form.Input 
              type="name" 
              name="emp_att_id" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_att_id
              })}
               value={this.state.emp_att_id}
              onChange={this.onChange}
              error={errors.emp_att_id}
              placeholder="Employee ATT ID" />
              {errors.emp_att_id && (
                    <div className="invalid-feedback">{errors.emp_att_id}</div>
                  )}
            </Form.Field>
            <Form.Field>
    
              <Form.Input type="name" 
              name="emp_sap_id" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_sap_id
              })}
              value={this.state.emp_sap_id}
              onChange={this.onChange}
              error={errors.emp_sap_id} 
              placeholder="Employee SAP ID" />
              {errors.emp_sap_id && (
                    <div className="invalid-feedback">{errors.emp_sap_id}</div>
                  )}
            </Form.Field>
            </Form.Group>
            <Form.Group >
            <Form.Field>
        
              <SelectListGroup 
              type="email"
               name="emp_location"
               className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_location
              })}  
               value={this.state.emp_location}
              onChange={this.onChange}
              options={emp_location}
              error={errors.emp_location} 
              placeholder="Employee Location" />
              {errors.emp_location && (
                    <div className="invalid-feedback">{errors.emp_location}</div>
                  )}
            </Form.Field>
            <Form.Field>
          
              <Form.Input type="email" 
              name="bill_rate"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.bill_rate
              })} 
              value={this.state.bill_rate}
              onChange={this.onChange}
              error={errors.bill_rate} 
              placeholder="Employee Bill Rate" />
              {errors.bill_rate && (
                    <div className="invalid-feedback">{errors.bill_rate}</div>
                  )}
            </Form.Field>
            <Form.Field>
             
              <SelectListGroup 
              type="email" 
              name="emp_level"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_level
              })}  
              value={this.state.emp_level}
              onChange={this.onChange}
              options={emp_level}
              error={errors.emp_level} 
              placeholder="Employee Level" />
              {errors.emp_level && (
                    <div className="invalid-feedback">{errors.emp_level}</div>
                  )}
            </Form.Field>
            </Form.Group>
            <Form.Group >
            <Form.Field>
              
              <SelectListGroup
              type="email" 
              name="emp_role"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_role
              })}
              value={this.state.emp_role}
              onChange={this.onChange}
              options={emp_role}
              error={errors.emp_role}
              placeholder="Employee Role" />
              {errors.emp_role && (
                    <div className="invalid-feedback">{errors.emp_role}</div>
                  )}
            </Form.Field>
            <Form.Field>
              
              <SelectListGroup 
              type="email" 
              name="emp_skills"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_skills
              })}
              value={this.state.emp_skills}
              onChange={this.onChange}
              error={errors.emp_skills}
              options={skills}
              placeholder="Employee Skills" />
              {errors.emp_skills && (
                    <div className="invalid-feedback">{errors.emp_skills}</div>
                  )}
                  
            </Form.Field>
            </Form.Group>
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button color="yellow" 
            type="submit"
            value="Submit"
            onClick={this.onSubmit}><Icon name='edit'></Icon>Edit Employee</Button>{' '}
            <Button color="secondary" onClick={this.editModal}>Exit</Button>
          </Modal.Actions>
        </Modal>
        <Modal open={this.state.deletemodal} trigger={<Button size='small' negative style={{ margin:'.5em' }} disabled={!this.state.isEnabledDeleteBtn} onClick={this.deleteModal}><Icon name='user delete'></Icon>Delete Employee</Button>} closeIcon basic centered={false}>
        
          <Modal.Header toggle={this.toggle}><Icon name='user delete'></Icon>Delete Employee</Modal.Header>
          <Modal.Content>

              <Form onSubmit={this.onSubmit}>
                Are you sure you want delete selected employee detail?
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button color="red" 
            type="submit"
            value="Submit"
            onClick={this.onSubmit}><Icon name='user delete'></Icon>Delete Employee</Button>{' '}
            <Button color="secondary" onClick={this.deleteModal}>Exit</Button>
          </Modal.Actions>
        </Modal>
      {/* {this.renderRedirect()} */}
      <BootstrapTable data={ employees }
        options={ options }
        selectRow={ selectRow }
        scrollTop={ 'Bottom' }
        Responsive
        headerStyle={ { background: 'rgba(51,51,51,0.425)', color: 'white' }}
        tableStyle={ { background: 'rgba(51,51,51,0.425)', margin:'.5em' } }
        bodyStyle={ { background: 'rgba(51,51,51,0.425)', margin:'.1em' } }
        csvFileName='Employees-List'
        striped
        condensed
        pagination
        hover
        exportCSV
        csvFileName='Employees-List.csv'
        search
        cellEdit={ cellEditProp }
        >
        
        <TableHeaderColumn 
        dataField='emp_att_id' 
        isKey={ true }
        dataSort={ true }>
        Emp ATT ID
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='emp_sap_id'
        dataSort={ true }>
         Emp SAP ID
         </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='emp_name'
        dataSort={ true }
         >
         Emp Name
         </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='emp_role'
        dataSort={ true }>
         Emp Role
         </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='emp_location'
        dataSort={ true }>
         Emp Located
         </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='emp_level'
        dataSort={ true }>
         Emp Level
         </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='emp_skills'
        dataSort={ true }
        >
         Emp Skills
         </TableHeaderColumn>

       
        <TableHeaderColumn 
        dataField='bill_rate' 
        dataSort={ true }
        width={90}>Bill Rate
        </TableHeaderColumn>
      </BootstrapTable>
      
      {/* <ToastContainer
          transition={Slide}
          autoClose={500}
        /> */}
      
      </div>
    )
  }
}


EmpTable.propTypes = {
  employee: PropTypes.object.isRequired,
  getEmployees: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  employee: state.employee,
  errors: state.errors
});

export default connect(mapStateToProps, { createEmployee, getEmployees,updateEmployee,deleteEmployee })(
  withRouter(EmpTable)
);
