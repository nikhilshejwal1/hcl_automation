import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { registerUser } from '../actions/authActions';
import { toast } from 'react-toastify';


class Register extends Component {

  constructor(){
    super();
    this.state = {
      name: '',
      sap_id: '',
      password: '',
      password2: '',
      att_id:'',
      emp_role:'',
      errors:{}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
      toast("You are already Registered", {
      
        autoClose: 5000
      });
    }
    
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
      toast("Please check the errors", {
      
        autoClose: 5000
      });
    }
  }

  onChange(e){
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e){
    e.preventDefault();
    
    const newUser ={
      name: this.state.name,
      sap_id: this.state.sap_id,
      att_id: this.state.att_id,
      password: this.state.password,
      password2: this.state.password2,
      emp_role: this.state.emp_role,
    
    };
    
    this.props.registerUser(newUser, this.props.history);
    
   // axios.post('/api/users/register', newUser)
   // .then(res=> console.log(res.data))
   // .catch(err => this.setState({ errors: err.response.data}));

  }

  render() {
    const { errors } = this.state;
    
    return (
      
      <div className="register">
    <div className="container">
      <div className="row">
        <div className="col-md-8 m-auto">
          <h1 className="display-4 text-center">Sign Up</h1>
          <p className="lead text-center">Create your account</p>
          <form noValidate onSubmit={this.onSubmit}>
            <div className="form-group">
              <input 
              type="text" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.name
              })}
              placeholder="Name"
              name="name" 
              value={this.state.name}
              onChange={this.onChange}
              />
              {errors.name && (
                    <div className="invalid-feedback">{errors.name}</div>
                  )}
            </div>
            <div className="form-group">
              <input 
              type="text" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.sap_id
              })} 
              placeholder="SAP ID" 
              name="sap_id" 
              value={this.state.sap_id}
              onChange={this.onChange}
              required />
              {errors.sap_id && (
                    <div className="invalid-feedback">{errors.sap_id}</div>
                  )}
            </div>
            <div className="form-group">
              <input 
              type="text" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.emp_role
              })} 
              placeholder="Employee Role" 
              name="emp_role" 
              value={this.state.emp_role}
              onChange={this.onChange}
              required />
              {errors.emp_role && (
                    <div className="invalid-feedback">{errors.emp_role}</div>
                  )}
            </div>
            <div className="form-group">
              <input 
              type="email" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.att_id
              })}
              placeholder="ATT Email ID" 
              name="att_id"
              value={this.state.att_id}
              onChange={this.onChange}
              />
              {errors.att_id && (
                    <div className="invalid-feedback">{errors.att_id}</div>
                  )}
            </div>
            <div className="form-group">
              <input 
              type="password" 
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.password
              })}
              placeholder="Password" 
              name="password"
              value={this.state.password}
              onChange={this.onChange}
              />
            </div>
            {errors.password && (
                    <div className="invalid-feedback">{errors.password}</div>
                  )}
            <div className="form-group">
              <input 
              type="password"
              className={classnames('form-control form-control-lg', {
                'is-invalid': errors.password2
              })} 
              placeholder="Confirm Password" 
              name="password2" 
              value={this.state.password2}
              onChange={this.onChange}
              />
              {errors.password2 && (
                    <div className="invalid-feedback">{errors.password2}</div>
                  )}
            </div>
            <input type="submit" className="btn btn-info btn-block mt-4" />
          </form>
        </div>
      </div>
    </div>
  </div>
    )
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { registerUser })(withRouter(Register));
