import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { loginUser } from '../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';
import { toast } from 'react-toastify';

class Login extends Component {

  constructor(){
    super();
    this.state = {
      att_id: '',
      password: '',
      errors:{}

    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
      toast("User Logged In", {
      
        autoClose: 5000
      });
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }


  onChange(e){
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e){
    e.preventDefault();
    
    const userData = {
      
      att_id: this.state.att_id,
      password: this.state.password
    };
    
    this.props.loginUser(userData);
   
  }

  render() {
    const { errors } =this.state;
    return (
      <div className="login">
    <div className="container">
      <div className="row">
        <div className="col-md-8 m-auto">
          <h1 className="display-4 text-center">Log In</h1>
          <p className="lead text-center">Sign in to your account</p>
          <form noValidate onSubmit={this.onSubmit}>

            <TextFieldGroup
              placeholder="Enter your ATT email ID" 
              value={this.state.att_id}
              onChange={this.onChange}
              name="att_id"
              errors={errors.att_id}
            />

             <TextFieldGroup
              placeholder="Password" 
              value={this.state.password}
              onChange={this.onChange}
              name="password"
              type="password"
              errors={errors.password}
            />

     
         
            <input type="submit" className="btn btn-info btn-block mt-4" />
          </form>
        </div>
      </div>
    </div>
  </div>
    )
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(Login);

