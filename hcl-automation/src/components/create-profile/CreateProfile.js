import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextFieldGroup from '../common/TextFieldGroup';
import SelectListGroup from '../common/SelectListGroup';
import { createProfile } from '../actions/profileActions';

class CreateProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
     
      sap_id: '',
      att_id: '',
      emp_role: '',
      emp_location: '',
      projects_assigned: '',
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const profileData = {
      name: this.state.name,
      sap_id: this.state.sap_id,
      att_id: this.state.att_id,
      projects_assigned: this.state.projects_assigned,
      emp_role: this.state.emp_role,
      emp_location: this.state.emp_location,
      
    };

    this.props.createProfile(profileData, this.props.history);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  

  render() {
    const { errors } = this.state;

  

    // Select options for status
    const options = [
      { label: '* Select Professional Status', value: 0 },
      { label: 'Delivery Head', value: 'Delivery Head' },
      { label: 'Delivery Manager', value: 'Delivery Manager' },
      { label: 'Senior Developer', value: 'Senior Developer' },
      { label: 'Manager', value: 'Manager' },
      { label: 'Student or Learning', value: 'Student or Learning' },
      { label: 'Instructor or Teacher', value: 'Instructor or Teacher' },
      { label: 'Intern', value: 'Intern' },
      { label: 'Other', value: 'Other' }
    ];

    return (
      <div className="create-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Create Your Profile</h1>
              <p className="lead text-center">
                Let's get some information to make your profile stand out
              </p>
              <small className="d-block pb-3">* = required fields</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup
                  placeholder="* ATT ID"
                  name="att_id"
                  value={this.state.att_id}
                  onChange={this.onChange}
                  error={errors.att_id}
                  info="Enter your ATT ID"
                />
                 <TextFieldGroup
                  placeholder="* SAP ID"
                  name="sap_id"
                  value={this.state.sap_id}
                  onChange={this.onChange}
                  error={errors.sap_id}
                  info="Enter your SAP ID"
                />
                <SelectListGroup
                  placeholder="Emp Role"
                  name="emp_role"
                  value={this.state.emp_role}
                  onChange={this.onChange}
                  options={options}
                  error={errors.emp_role}
                  info="Give us an idea of where you are at in your career"
                />
             
                
                <TextFieldGroup
                  placeholder="Location"
                  name="emp_location"
                  value={this.state.emp_location}
                  onChange={this.onChange}
                  error={errors.emp_location}
                  info="City or city & state suggested (eg. Boston, MA)"
                />
                <TextFieldGroup
                  placeholder="* Projects Assigned"
                  name="projects_assigned"
                  value={this.state.projects_assigned}
                  onChange={this.onChange}
                  error={errors.projects_assigned}
                  info="Please use comma separated values (eg.
                    HTML,CSS,JavaScript,PHP"
                />

                <div className="mb-3">
                  <button
                    type="button"
                    onClick={() => {
                      this.setState(prevState => ({
                        displaySocialInputs: !prevState.displaySocialInputs
                      }));
                    }}
                    className="btn btn-light"
                  >
                    
                  </button>
                  <span className="text-muted"></span>
                </div>
                
                <input
                  type="submit"
                  value="Submit"
                  className="btn btn-info btn-block mt-4"
                />

              </form>
            </div>
          </div>
        </div>
      </div>
      
    );
  }
}

CreateProfile.propTypes = {
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors: state.errors
});

export default connect(mapStateToProps, { createProfile })(
  withRouter(CreateProfile)
);
