import axios from 'axios';

import {
  GET_PROJECT,
  GET_PROJECT_BY_PMT_ID,
  GET_PROJECTS,
  PROJECT_LOADING,
  GET_ERRORS,
  CREATE_PROJECT,
  UPDATE_PROJECT,
  DELETE_PROJECT
} from './types';


// Create Project
export const createProject = (projData, history) => dispatch => {
  axios
    .post('/api/projects', projData)
    .then(res => 
           {
           dispatch({
             type: CREATE_PROJECT,
             payload: res.data
           })
         })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Update Project
export const updateProject = (id,projData,history) => dispatch => {
    axios
      .put(`/api/projects/${id}`,projData)
      .then(res => 
        {
        dispatch({
          type: UPDATE_PROJECT,
          payload: res.data
        })
      }
      )
      .catch(err =>
        dispatch({
          type: GET_ERRORS,
          payload: err.response.data
        })
      );
  };
  
  

// Delete Project
export const deleteProject = id => dispatch => {
  axios
    .delete(`/api/projects/${id}`)
    .then(res =>
      {
      dispatch({
        type: DELETE_PROJECT,
        payload: res.data
      })}
    )
    .catch(err =>
      {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    }
    );
};

// Project loading
export const setProjectLoading = () => {
  return {
    type: PROJECT_LOADING
  };
};

// Get all projects
export const getProjects = () => dispatch => {
  dispatch(setProjectLoading());
  axios
    .get('/api/projects/all')
    .then(res => {
      console.log('get projects==>',res);
      dispatch({
        type: GET_PROJECTS,
        payload: res.data
      })
    }
      
    )
    .catch(err =>
      dispatch({
        type: GET_PROJECTS,
        payload: null
      })
    );
};



//Get project by PMT ID
export const getProjectsByPmtId = pmt_id => dispatch => {
  dispatch(setProjectLoading());
  axios
    .get(`/api/projects/pmt_id/${pmt_id}`)
    .then(res => {
      console.log('get projects by PMT ID==>',res);
      dispatch({
        type: GET_PROJECT,
        payload: res.data
      })
    }
      
    )
    .catch(err =>
      dispatch({
        type: GET_PROJECT,
        payload: null
      })
    );
};
