import axios from 'axios';

import {
  GET_PROFILE,
  GET_PROFILES,
  GET_PROJECTS_LIST,
  PROFILE_LOADING,
  CLEAR_CURRENT_PROFILE,
  SET_CURRENT_USER,
  GET_ERRORS
} from './types';

// Get current profile
export const getCurrentProfile = () => dispatch => {
  dispatch(setProfileLoading());
  axios
    .get('/api/profile')
    .then(res =>
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_PROFILE,
        payload: {}
      })
    );
};

// Create MY Project
export const createMyProject = (myprojectData, history) => dispatch => {
  axios
    .post('/api/profile/project', myprojectData)
    .then(res => history.push('/dashboard')) 
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
      
    );
};

// Get Projects from Projects List
export const getProjectsList = (inputValue) => dispatch => {
  dispatch(setProfileLoading());
  axios
    .get(`/api/projects/pmt_id/${inputValue}`)
    .then(res =>
      dispatch({
        type: GET_PROJECTS_LIST,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_PROJECTS_LIST,
        payload: {}
      })
    );
};

// Create Profile
export const createProfile = (profileData, history) => dispatch => {
  axios
    .post('/api/profile', profileData)
    .then(res => history.push('/dashboard'))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Profile loading
export const setProfileLoading = () => {
  return {
    type: PROFILE_LOADING
  };
};

// Get all profiles
export const getProfiles = () => dispatch => {
  dispatch(setProfileLoading());
  axios
    .get('/api/profile/all')
    .then(res =>{
      console.log('profiles',res);
      dispatch({
        type: GET_PROFILES,
        payload: res.data
      })
    }
      
    )
    .catch(err =>
      dispatch({
        type: GET_PROFILES,
        payload: null
      })
    );
};

// Delete Profile Projects
export const deleteProject = id => dispatch => {
  axios
    .delete(`/api/profile/projects/${id}`)
    .then(res =>
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Clear profile
export const clearCurrentProfile = () => {
  return {
    type: CLEAR_CURRENT_PROFILE
  };
};
