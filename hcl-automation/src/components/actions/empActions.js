import axios from 'axios';
import {  toast } from 'react-toastify';

import {
  GET_EMPLOYEE,
  GET_EMPLOYEES,
  EMPLOYEE_LOADING,
  CREATE_EMPLOYEE,
  UPDATE_EMPLOYEE,
  DELETE_EMPLOYEE,
  GET_ERRORS
} from './types';

//create employee
export const createEmployee = (employeeData, history) => dispatch => {
  
  axios
    .post('/api/employees', employeeData)
    .then(res =>{
      dispatch({
        type: CREATE_EMPLOYEE,
        payload: res.data
      })
    }  
    
  )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Employee loading
export const setEmployeeLoading = () => {
  return {
    type: EMPLOYEE_LOADING
  };
};
// Update Employee
export const updateEmployee = (id,employeeValue,history) => dispatch => {
  axios
    .put(`/api/employees/${id}`,employeeValue)
    .then(res => 
      {
      dispatch({
        type: UPDATE_EMPLOYEE,
        payload: res.data
      })
    }
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};
  
// Delete Employee
export const deleteEmployee = (id,history) => dispatch => {
  axios
    .delete(`/api/employees/${id}`)
    .then(res => 
     { 
      dispatch({
        type: DELETE_EMPLOYEE,
        payload: res.data
      })}
    )
    .catch(err =>
      {
        console.log("Delete error sis : ",err);
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    }
    );
};

// Get all employees
export const getEmployees = () => dispatch => {
  dispatch(setEmployeeLoading());
  axios
    .get('/api/employees/all')
    .then(res => {
      
      dispatch({
        type: GET_EMPLOYEES,
        payload: res.data
      })
    }
      
    )
    .catch(err =>
      dispatch({
        type: GET_EMPLOYEES,
        payload: null
      })
    );
};


