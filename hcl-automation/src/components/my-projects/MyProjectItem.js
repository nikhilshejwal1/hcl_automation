import React from 'react';
import TextFieldGroup from '../common/TextFieldGroup';
import { Card, Tab, Button, Input } from 'semantic-ui-react'

const MyProjectItem = (props) => {
 
 
  return (
    <form onSubmit={props.getProject}>
      <Input focus style={{ display:"block" }} placeholder="Enter PMT ID Here" type="text" name="pmt_id"/>
      <br></br>
      <Button size='tiny' color='green'>Submit</Button>
    </form>
  );
}

export default MyProjectItem;