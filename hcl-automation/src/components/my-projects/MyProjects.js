import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter,Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Card, Tab, Button, Segment, Icon } from 'semantic-ui-react'
import TextFieldGroup from '../common/TextFieldGroup';
import { getProjects } from '../actions/projActions';
import { getProjectsList } from '../actions/profileActions';
import axios from 'axios';  
import MyProjectsItem from './MyProjectItem';
import { createMyProject } from '../actions/profileActions';
import { deleteProject } from '../actions/profileActions';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css' 



const initialState = {
  project_name: '',
  project_title: '',
  query:'',
  modal: false,
  inputValue:''
}
class MyProjects extends Component {

  deleteAlert = (id) => {
    confirmAlert({
      message: 'All the data related to the project including estimations, resouce loading will be lost! Are you sure you want to delete it?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => this.props.deleteProject(id)
        },
        {
          label: 'No',
        }
      ]
    })
  }
  
  // ondeleteProject(id){
    
  // }
  
  getProject = (e) => {
    e.preventDefault();
    const user = e.target.elements.pmt_id.value;
    if (user) {
      axios.get(`api/projects/pmt_id/${user}`)
      .then(res => {
        
        const project_pmt_id = res.data.pmt_id;
        this.setState({ project_pmt_id });
        const project_title = res.data.project_title;
        this.setState({ project_title });
        const project_program = res.data.project_program;
        this.setState({ project_program });
        const project_pid = res.data.project_pid;
        this.setState({ project_pid });
        const project_att_manager = res.data.project_att_manager;
        this.setState({ project_att_manager });
        const project_att_manager_id = res.data.project_att_manager_id;
        this.setState({ project_att_manager_id });
        const project_cr_no = res.data.project_cr_no;
        this.setState({ project_cr_no });
        const project_sr_no = res.data.project_sr_no;
        this.setState({ project_sr_no });
        const project_project_type = res.data.project_project_type;
        this.setState({ project_project_type });
        const const_non_const = res.data.const_non_const;
        this.setState({ const_non_const });
      })
    } else return;
  }
  constructor(props) {
    super(props);
    this.state = initialState;

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.toggle = this.toggle.bind(this);
  }

   componentDidMount() {
    // this.props.getProjects();
    // this.props.deleteProject();
    
  }
  toggle = ()=> {
    this.setState({
      modal: !this.state.modal
    });
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

  const myprojectData = {
    project_pmt_id: this.state.project_pmt_id,
    project_title: this.state.project_title,
     project_pid: this.state.project_pid,
     project_att_manager: this.state.project_att_manager,
     project_att_manager_id: this.state.project_att_manager_id,
     project_cr_no: this.state.project_cr_no,
     project_sr_no: this.state.project_sr_no,
     project_program: this.state.project_program,
     const_non_const: this.state.const_non_const,
     start_date: this.state.start_date,
     end_date: this.state.end_date,
     m_5: this.state.m_5,
     m_7: this.state.m_7,
     m_9: this.state.m_9,
     warranty: this.state.warranty,
     project_project_type: this.state.project_project_type
     
  };
  toast("Project added to your Profile!");
  this.props.createMyProject(myprojectData, this.props.history);
  this.setState({
    
    modal:false
  })

}

  render() {
    const { errors } = this.state;
    const { project } = this.props;
  

    const projects = this.props.projects.map(pro => 

    <Card className='project_card'>
      <Card.Content key={pro._id}>
        <Card.Header className='card-text'> {pro.project_title}</Card.Header>
        <Card.Meta className='card-text'>ATT Manager ID: {pro.project_att_manager_id}</Card.Meta>
        <Card.Description className='card-text'>{pro.project_att_manager} and PMT ID is {pro.project_pmt_id}</Card.Description>
        <Card.Content extra>
        <div className='ui three buttons'>
        <Link to={`/projects/pmt_id/${pro.project_pmt_id}`}>
        //link to resource loading
          <Button compact size='tiny' basic color='green'>
            Add People
          </Button>
          </Link>
          <Button compact size='tiny' basic color='blue'>
            Edit Project
          </Button>
          <Button compact size='tiny' basic color='red' onClick={this.deleteAlert.bind(this, pro._id)}>
            Delete
          </Button>
        </div>
      </Card.Content>
      </Card.Content>
    </Card>


    

    );

    return (
      <div>
   
        <Button color="secondary" style={{ margin:'.5em'}} onClick={this.toggle}>Add My Projects</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader>Add to my projects</ModalHeader>
          <ModalBody>
          <MyProjectsItem getProject={this.getProject} />
            { this.state.project_title ? <p name={"project_title"} value={this.state.project_title} ></p> : <small>Please enter a PMT ID.</small> }
          <form onSubmit={this.onSubmit}>

            <Segment.Group raised>
            <Segment inverted secondary>Project PMT ID: {this.state.pmt_id}</Segment>
            <Segment inverted secondary>Project Title: {this.state.project_title}</Segment>
            <Segment inverted secondary>Project Program: {this.state.project_program}</Segment>
            <Segment inverted secondary>Project PID: { this.state.project_pid }</Segment>
          <Segment inverted secondary>Project ATT Manager: { this.state.project_att_manager }</Segment>
          <Segment inverted secondary>Project ATT Manager ID: { this.state.project_att_manager_id }</Segment>
          <Segment inverted secondary>Project CR No: { this.state.project_cr_no }</Segment>
          <Segment inverted secondary>Project SR No:{ this.state.project_sr_no }</Segment>
          <Segment inverted secondary>Construction/Non-Construction: { this.state.const_non_const }  </Segment>   

            </Segment.Group>
      </form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" type="submit"
            value="Submit"
            onClick={this.onSubmit}>Add to My Projects</Button>
            <ToastContainer/>
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      
        <Card.Group>
           
           {projects}
           
        </Card.Group>  
      </div>
    )
  }
}



MyProjects.propTypes = {
  profile: PropTypes.object.isRequired,
  createMyProject: PropTypes.func.isRequired,
  deleteProject: PropTypes.func.isRequired,
  getProjectsList: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  project: state.project,
  profile: state.profile,
  errors: state.errors
});

export default connect(mapStateToProps, { getProjectsList, deleteProject, createMyProject })(
  withRouter(MyProjects)
);