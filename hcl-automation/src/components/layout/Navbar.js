import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../actions/authActions';
import { clearCurrentProfile } from '../actions/profileActions';
import { getCurrentProfile } from '../actions/profileActions';
import { toast } from 'react-toastify';

class Navbar extends Component {
  componentDidMount() {
    this.props.getCurrentProfile();
    
  }
  onLogoutClick(e) {
    e.preventDefault();
    this.props.logoutUser();
    this.props.clearCurrentProfile();
    
    
  }

  render() {
    const { isAuthenticated, user } = this.props.auth;
    const { profile, loading } = this.props.profile;

    let navbarContent;

    if ( profile === null){
      navbarContent = (
        <li className="nav-item">
          <Link className="nav-link" to="/edit-profile">
            Create Profile
          </Link>
        </li>
      );
    }
    else{
      if( profile.emp_role == 'Delivery Head' || 
          profile.emp_role == 'Delivery Manager' ||
          profile.emp_role == 'Intern'   ){
     navbarContent = (
      <li className="nav-item">
          <Link className="nav-link" to="/edit-profile">
            Edit Profile
          </Link>
        </li>
    );
  }
  else{
    navbarContent = (
      
          <Link className="nav-link" to="/create-profile">
            Create Profile
          </Link>
       
    );
  }
  }
    const authLinks = (
      <ul className="navbar-nav ml-auto">
      <li className="nav-item">
          <Link className="nav-link" to="/dashboard">
            Dashboard
          </Link>
        </li>
        <li className="nav-item">
          {navbarContent}
        </li>
      <li className="nav-item">
        <a
          href=""
          onClick={this.onLogoutClick.bind(this)}
          className="nav-link"
        >
        
         {' '}
          Logout {user.name}
        </a>
      </li>
    </ul>
    );

    const guestLinks = (
      <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/Register">Sign Up</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/login">Login</Link>
          </li>
        </ul>
    );

    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-4">
    <div className="container">
      <Link className="navbar-brand" to="/">HCL America</Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile-nav">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="mobile-nav">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/profiles"> 
            {' '}
            Delivery Managers
            </Link>
          </li>
        </ul>
        {isAuthenticated ? authLinks : guestLinks}
        
      </div>
    </div>
  </nav>

    )
  }
}

Navbar.propTypes = {
  getCurrentProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  auth: state.auth
});

export default connect(mapStateToProps, { logoutUser, clearCurrentProfile, getCurrentProfile })(Navbar);
