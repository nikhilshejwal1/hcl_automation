import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import isEmpty from '../validation/is-empty';


class ProfileItem extends Component {
  render() {
    const { profile } = this.props;

    return (
      <div className="card card-body bg-dark mb-3">
        <div className="row">
          <div className="col-2">
            <h2>{profile.user.name}</h2>
          </div>
          <div className="col-lg-6 col-md-4 col-8">
            <h3>{profile.att_id}</h3>
            <p>
              {profile.status}{' '}
              {isEmpty(profile.emp_role) ? null : (
                <span>at {profile.emp_role}</span>
              )}
            </p>
            <p>
              {isEmpty(profile.emp_location) ? null : (
                <span>{profile.emp_location}</span>
              )}
            </p>
            <Link to={`/profile/${profile.att_id}`} className="btn btn-info">
              View Profile
            </Link>
          </div>
          <div className="col-md-4 d-none d-md-block">
            <h4>Projects Assigned</h4>
            <ul className="list-group">
              {profile.projects_assigned.slice(0, 4).map((skill, index) => (
                <li key={index} className="list-group-item">
                  <i className="fa fa-check pr-1" />
                  {profile.projects_assigned}
                </li>
              ))}
            </ul>
          </div>
          
        </div>
      </div>
    );
  }
}

ProfileItem.propTypes = {
  profile: PropTypes.object.isRequired,
 
};

export default ProfileItem;
