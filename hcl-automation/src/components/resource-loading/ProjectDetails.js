import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import ResourceLoading from './ResourceLoading';
import { Tab,Icon } from 'semantic-ui-react'
import { TabContent, TabPane, Nav, NavItem, NavLink, Button, CardTitle, CardText, Row, Col, Fade,Card } from 'reactstrap';
import classnames from 'classnames';
import Dashboard from '../dashboard/Dashboard';
import { getProjectsByPmtId } from '../actions/projActions';
import { getCurrentProfile } from '../actions/profileActions';



 class ProjectDetails extends Component {
   componentDidMount(){
     this.props.getCurrentProfile();
   }
  
  componentWillReceiveProps(){
    if(this.props.match.params.pmt_id){
      this.props.getCurrentProfile(this.props.match.params.pmt_id);
    }
  }

  

  componentWillReceiveProps(nextProps) {
   
  }
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render() {
    
    

 const { profile, loading } = this.props.profile;
    
    return (
      <div>
     
     <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}
            >
              Resource Loading
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}
            >
              Moar Tabs
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
              <br/>
            <h2>Resource loading for PMT ID - {this.props.match.params.pmt_id} </h2>
                <ResourceLoading pmt_id={this.props.match.params.pmt_id} profile={profile}/>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Dashboard/>
          </TabPane>
        </TabContent>
        
      </div>
    )
  }
}

ProjectDetails.propTypes = {
  profile: PropTypes.object.isRequired,
  getCurrentProfile: PropTypes.func.isRequired
 
};

const mapStateToProps = state => ({
  profile: state.profile
  

});

export default connect(mapStateToProps, { getCurrentProfile })(
  (ProjectDetails)
);