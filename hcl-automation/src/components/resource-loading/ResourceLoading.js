import React, { Component } from 'react'
import ProjectDetails from './ProjectDetails';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { getProjectsByPmtId } from '../actions/projActions';
import {BootstrapTable, TableHeaderColumn, DeleteButton} from 'react-bootstrap-table';
import axios from 'axios';


 class ResourceLoading extends Component {
  
  state = {
    proj_total_person_month: null
  }
  
  // componentDidMount(){
  //   if(this.props.match.params.pmt_id){
  //     this.props.getProjectsByPmtId(this.props.match.params.pmt_id);
  //   }
  // }
 
  render() {
    console.log(this.props, 'pmt_id')
    const { profile } = this.props;
    const products = profile.projemployee;

  const options = {
    noDataText: 'This is custom text for empty data'
    // withoutNoDataText: true, // this will make the noDataText hidden, means only showing the table header
  };
  const selectRow = {
    mode: 'checkbox',
    bgColor: 'rgb(238, 193, 213)'
  };
  
    
  
    const cellEdit = {
      mode: 'click'
    };
    return (
      <div>
       
        <BootstrapTable data={ products }
        selectRow={ selectRow }
        headerStyle={ { background: 'rgba(51,51,51,0.425)', color: 'white' }}
        tableStyle={ { background: 'rgba(51,51,51,0.425)', margin:'.5em' } }
        bodyStyle={ { background: 'rgba(51,51,51,0.425)', margin:'.1em' } }
        striped
        pagination
        deleteRow
        exportCSV
        csvFileName='Projects-List.csv'
        search
        cellEdit={ cellEdit }
        insertRow deleteRow exportCSV>
       <TableHeaderColumn 
        dataField='proj_emp_att_id' 
        width={140}
        isKey={ true }
        dataSort={ true }>
        Emp ATT ID
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='proj_emp_sap_id'
        width={140}
        dataSort={ true }>
         Emp SAP ID
         </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='proj_empname'
        width={140}
        dataSort={ true }
         >
         Emp Name
         </TableHeaderColumn>

         <TableHeaderColumn 
        dataField='proj_emprole'
        width={140}
        dataSort={ true }>
         Emp Role
         </TableHeaderColumn>

          <TableHeaderColumn 
        dataField='proj_emp_pmt_id'
        width={140}
        dataSort={ true }>
         PMT ID is{this.props.pmt_id}
         </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='proj_emp_bill_rate' 
        dataSort={ true }
        width={90}>Bill Rate
        </TableHeaderColumn>
        
        <TableHeaderColumn 
        dataField='proj_total_person_month' 
        dataSort={ true }
        width={140}>Total Person Month
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='sept18' 
        dataSort={ true }
        width={80}>Sept 18
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='oct18' 
        dataSort={ true }
        width={80}>Oct 18
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='nov18' 
        dataSort={ true }
        width={80}>Nov 18
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='dec18' 
        dataSort={ true }
        width={80}>Dec 18
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='jan19' 
        dataSort={ true }
        width={80}>Jan 19
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='feb19' 
        dataSort={ true }
        width={80}>Feb 19
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='mar19' 
        dataSort={ true }
        width={80}>Mar 19
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='april19' 
        dataSort={ true }
        width={80}>Apr 19
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='may19' 
        dataSort={ true }
        width={80}>May 19
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='june19' 
        dataSort={ true }
        width={80}>June 19
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='july19' 
        dataSort={ true }
        width={80}>July 19
        </TableHeaderColumn>

        <TableHeaderColumn 
        dataField='aug19' 
        dataSort={ true }
        width={80}>Aug 19
        </TableHeaderColumn>


        <TableHeaderColumn 
        dataField='sept19' 
        dataSort={ true }
        width={80}>Sept 19
        </TableHeaderColumn>

      </BootstrapTable>
      </div>
    )
  }
}

export default ResourceLoading;