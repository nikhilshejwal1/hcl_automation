import React, { Component } from 'react'
import { TabContent, TabPane, Nav, NavItem, NavLink, Button, CardTitle, CardText, Row, Col, Fade } from 'reactstrap';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import EmpTable from '../emp-table/EmpTable';
import ProjTable from '../proj-table/ProjTable';
import { Modal } from 'react-bootstrap';
import { getCurrentProfile } from '../actions/profileActions'
import { Card, Tab, Icon } from 'semantic-ui-react'
import MyProjects from '../my-projects/MyProjects';
import { toast, cssTransition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'; 

// const panes = [
//   { menuItem: 'Tab 1', render: () => <Tab.Pane><EmpTable/></Tab.Pane> },
//   { menuItem: 'Tab 2', render: () => <Tab.Pane>Tab 2 Content</Tab.Pane> },
//   { menuItem: 'Tab 3', render: () => <Tab.Pane>Tab 3 Content</Tab.Pane> },
// ]

const Zoom = cssTransition({
  enter: 'zoomIn',
  // zoomIn will become zoomOut--top-right or zoomOut--top-left and so on
  exit: 'zoomOut',
  // default to false
  appendPosition: true
});
const cellEditProp = {
  mode: 'click',
  blurToSave: true
};
class Tabsdash extends Component {
  
  componentDidMount() {
    this.props.getCurrentProfile();
    
    // toast("Welcome Back", {
    //   autoClose: 2000
    // });

  }

  createCustomToolBar = props => {
    return (
      <div style={ { margin: '15px' } }>
        { props.components.btnGroup }
        <div className='col-xs-8 col-sm-4 col-md-4 col-lg-2'>
          { props.components.searchPanel }
        </div>
      </div>
    );
  }


  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    
    this.state = {
      activeTab: '1',
    };
  }

  
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
        
      });
    }
  }

  render() {

  const { profile, loading } = this.props.profile;
  const { user } = this.props.auth;
  
  const selectRow = {
    mode: 'checkbox',
    showOnlySelected: true
  };
  const options = {
    toolBar: this.createCustomToolBar
  };
    return (
      <div>
        
        <Nav  tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}
            >
              <Icon name='users'></Icon> Employees
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}
            >
             <Icon name='line graph'></Icon> Projects
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
            className={classnames({ active: this.state.activeTab === '3' })}
              onClick={() => { this.toggle('3'); }}
            >
             <Icon name='pie chart'></Icon> My Projects and Resources
              </NavLink>
            </NavItem>
        </Nav>
        <TabContent   activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col >
              
                    <EmpTable/>
             
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
            <Col sm="12">
            
            <ProjTable/>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="3">
            <Row>
            <Col sm="12">
            
            <MyProjects projects = {profile.projects} />
  {/* <Tab menu={{ fluid: true, vertical: true, tabular: true }} panes={panes} /> */}
              </Col>
            </Row>
          </TabPane>
        </TabContent>
      </div>
    )
  }
}

Tabsdash.propTypes = {
  getCurrentProfile: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired
  
};

const mapStateToProps = state => ({
  profile: state.profile,
  auth: state.auth
});

export default connect(mapStateToProps, { getCurrentProfile })(
  Tabsdash
);
