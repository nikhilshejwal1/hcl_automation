import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import { Provider } from 'react-redux';
import store from './store';
import jwt_decode from 'jwt-decode';
import setAuthToken from './components/utils/setAuthToken';
import { setCurrentUser, logoutUser } from './components/actions/authActions';
import Navbar from './components/layout/Navbar';
import Footer from './components/layout/Footer';
import Landing from './components/layout/Landing';
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import Dashboard from './components/dashboard/Dashboard';
import Tabsdash from './components/dashboard/Tabsdash';
import { clearCurrentProfile } from './components/actions/profileActions';
import PrivateRoute from './components/common/PrivateRoute';
import CreateProfile from './components/create-profile/CreateProfile';
import EditProfile from './components/edit-profile/EditProfile';
import Profiles from './components/displayprofiles/Profiles';
import EmpTable from './components/emp-table/EmpTable';
import ProjectDetails from './components/resource-loading/ProjectDetails';


// Check for token
if (localStorage.jwtToken) {
  // Set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // TODO: Clear current Profile
    store.dispatch(clearCurrentProfile());
    // Redirect to login
    window.location.href = '/login';
  }
}

class App extends Component {
  render() {
    return (
      <Provider store={ store } >
      <Router>
      <div className="App">
       
        <Navbar />
        <Route exact path="/" component={ Landing } />
        <div className="container">
        <Route exact path="/login" component={ Login } />
        <Route exact path="/Register" component={ Register } />
        <Route exact path="/Profiles" component={ Profiles } />
    <Route exact path="/projects/pmt_id/:pmt_id" render={(props) => <ProjectDetails {...props} />} />
    
       <Switch>
                <PrivateRoute exact path="/dashboard" component={Dashboard} />
                <PrivateRoute exact path="/tabsdash" component={Tabsdash} />
           
             </Switch>
             <Switch>
                <PrivateRoute
                  exact
                  path="/create-profile"
                  component={CreateProfile}
                />
          </Switch>

          <Switch>
                <PrivateRoute
                  exact
                  path="/edit-profile"
                  component={EditProfile}
                />
          </Switch>
              
        </div>
        <Footer />

      </div>
      </Router>
      </Provider>
    );
  }
}

export default App;
