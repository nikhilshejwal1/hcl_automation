const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

//Load Validation
const validateProfileInput = require('../../validation/profile');
const validateProjectInput = require('../../validation/projects');
const validateEducationInput = require('../../validation/education');

//Load Profile Model
const Profile = require('../../models/Profile');

//Load User Model
const User = require('../../models/User');

//Load Project Model
const Project = require('../../models/Project');


// @route   GET api/profile/test
// @desc    Tests profile route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Profile Works' }));

// @route GET api/profile
//@desc   GET current users profile
//@access Private

router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const errors = {};

    Profile.findOne({ user: req.user.id })
      .populate('user', ['att_id', 'sap_id', 'emp_location'])
      .then(profile => {
        if (!profile) {
          errors.noprofile = 'There is no profile for this user';
          return res.status(404).json(errors);
        }
        res.json(profile);
      })
      .catch(err => res.status(404).json(err));
  }
);

// @route   GET api/profile/all
// @desc    Get all profiles
// @access  Public
router.get('/all', (req, res) => {
  const errors = {};

  Profile.find()
    .populate('user', ['att_id', 'sap_id', 'emp_location' , 'emp_role'])
    .then(profiles => {
      if (!profiles) {
        errors.noprofile = 'There are no profiles';
        return res.status(404).json(errors);
      }

      res.json(profiles);
    })
    .catch(err => res.status(404).json({ profile: 'There are no profiles' }));
});


// @route GET api/profile/att_id
//@desc   GET profile by att_id
//@access Public
router.get('/att_id/:att_id', (req, res) => {
  const errors = {};

  Profile.findOne({ att_id: req.params.att_id })
    .populate('user', ['att_id', 'sap_id'])
    .then(profile => {
      if (!profile) {
        errors.noprofile = 'There is no profile for this user';
        res.status(404).json(errors);
      }

      res.json(profile);
    })
    .catch(err => res.status(404).json(err));
});

// @route   POST api/profile/project
// @desc    Add project to profile
// @access  Private
router.post(
  '/project',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    // const { errors, isValid } = validateProfileProjectInput(req.body);

    // // Check Validation
    // if (!isValid) {
    //   // Return any errors with 400 status
    //   return res.status(400).json(errors);
    // }

    Profile.findOne({ user: req.user.id }).then(profile => {
      const newPro = {
     
     project_pmt_id : req.body.project_pmt_id,  
     project_title: req.body.project_title,
     project_pid: req.body.project_pid,   
     project_att_manager: req.body.project_att_manager,
     project_att_manager_id: req.body.project_att_manager_id,
     project_cr_no: req.body.project_cr_no,
     project_sr_no: req.body.project_sr_no,
     project_program: req.body.project_program,
     const_non_const: req.body.const_non_const,
     m_5: req.body.m_5,
     m_7: req.body.m_7,
     m_9: req.body.m_9,
     warranty: req.body.warranty,
     project_project_type: req.body.project_project_type 
        
      };
     

      // Add to exp array
      profile.projects.unshift(newPro);
     

      profile.save().then(profile => res.json(profile));
    });
  }
);

// @route   POST api/posts/comment/:id
// @desc    Add comment to post
// @access  Private
router.post(
  '/project/employee/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    // const { errors, isValid } = validatePostInput(req.body);

    // // Check Validation
    // if (!isValid) {
    //   // If any errors, send 400 with errors object
    //   return res.status(400).json(errors);
    // }

    Profile.findOne({ user: req.user.id })
      .then(profile => {
        const newProjEmp = {
          proj_empname : req.body.proj_empname,  
          proj_emp_att_id: req.body.proj_emp_att_id,
          proj_emp_sap_id: req.body.proj_emp_sap_id,   
          proj_emp_bill_rate: req.body.proj_emp_bill_rate,
          proj_emprole: req.body.proj_emprole,
          proj_emp_pmt_id: req.body.proj_emp_pmt_id,
          proj_total_person_month: req.body.proj_total_person_month,
          sept18 : req.body.sept18,
          oct18 : req.body.oct18,
          nov18 : req.body.nov18,
          dec18 : req.body.dec18,
          jan19 : req.body.jan19,
          feb19 : req.body.feb19,
          mar19 : req.body.mar19,
          april19 : req.body.april19,
          may19 : req.body.may19,
          june19 : req.body.june19,
          july19 : req.body.july19,
          aug19 : req.body.aug19,
          sept19 : req.body.sept19,
        };

        // Add to comments array
        profile.projemployee.unshift(newProjEmp);

        // Save
        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(404).json({ postnotfound: 'No project found' }));
  }
);


router.put(
  '/education/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateEducationInput(req.body);

    // Check Validation
    if (!isValid) {
      // Return any errors with 400 status
      return res.status(400).json(errors);
    }

    Profile.findOneAndUpdate({ user: req.user.id }).then(profile => {
      const newEdu = {
        school: req.body.school,
        degree: req.body.degree,
        fieldofstudy: req.body.fieldofstudy,
        from: req.body.from,
        to: req.body.to,
        current: req.body.current,
        description: req.body.description
      };

      // Add to exp array
      profile.education.unshift(newEdu);

      profile.save().then(profile => res.json(profile));
    });
  }
);

// @route   DELETE api/profile/experience/:_id
// @desc    Delete experience from profile
// @access  Private
router.delete(
  '/projects/:pro_id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id })
      .then(profile => {
        // Get remove index
        const removeIndex = profile.projects
          .map(item => item.id)
          .indexOf(req.params.pro_id);

        // Splice out of array
        profile.projects.splice(removeIndex, 1);

        // Save
        profile.save().then(profile => res.json(profile));
      })
      .catch(err => res.status(404).json(err));
  }
);

// @route   DELETE api/profile/experience/:_id
// @desc    Delete experience from profile
// @access  Private
router.get(
  '/projects/:project_title',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id })
      .then(profile => {
        // Get remove index
        const removeIndex = profile.projects
          .map(item => item.id)
          .indexOf(req.params.pro_id);

        // Splice out of array
        profile.projects.slice(removeIndex,1);

        // Save
        profile.save().then(profile => res.json(profile.projects));
      })
      .catch(err => res.status(404).json(err));
  }
);


// @route POST api/profile
//@desc   POST create users profile
//@access Private


router.post(
  '/',
  passport.authenticate('jwt', {session: false}),
  (req, res) => {

    const { errors, isValid } = validateProfileInput(req.body);
    if(!isValid){
      //Return any error with 400
      return res.status(400).json(errors);
    }

    //Get fields

    const profileFields = {};
    profileFields.user = req.user.id;
    if (req.body.att_id) profileFields.att_id = req.body.att_id;
    if (req.body.sap_id) profileFields.sap_id = req.body.sap_id;
    if (req.body.emp_location) profileFields.emp_location = req.body.emp_location;
    if (req.body.emp_role) profileFields.emp_role = req.body.emp_role;
    
    // Project assign - Spilt into array
    if (typeof req.body.projects_assigned !== 'undefined') {
      profileFields.projects_assigned = req.body.projects_assigned.split(',');
    }
    

    Profile.findOne({ user: req.user.id })
    .then(profile => {
      if(profile) {
        //Update 
        Profile.findOneAndUpdate(
          { user: req.user.id },
          { $set: profileFields },
          { new: true }
        )
        .then(profile => res.json(profile));
      }else{
        //Create


        //Check if att id exists
        Profile.findOne({ att_id: profileFields.att_id }).then(profile => {
          if (profile) {
            errors.att_id = 'That att id already exists';
            res.status(400).json(errors);
          }

          // Save Profile
          new Profile(profileFields).save().then(profile => res.json(profile));
        });
      }
    });
  }
);



module.exports = router;
