const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const Project = require('../../models/Project');
const Profile = require('../../models/Profile');
const User = require('../../models/User');
const Employee = require('../../models/Employee');

const validateEmployeeInput = require('../../validation/employee');

// This is main file for employee

// @route   GET api/posts/test
// @desc    Tests post route
// @access  Public


router.get('/all', (req, res) => {
  const errors = {};

  Employee.find()
    .populate('user', ['emp_name', 'emp_att_id'])
    .then(employee => {
      if (!employee) {
        errors.noemployee = 'There are no Employee';
        return res.status(404).json(errors);
      }

      res.json(employee);
    })
    .catch(err => res.status(404).json({ employee: 'There are no Employees' }));
});

router.put('/:_id', passport.authenticate('jwt', { session: false}),(req, res)=>{
  var query = {"_id":req.params._id},
  updateData = {"$set":{
    emp_name: req.body.emp_name,
     emp_location: req.body.emp_location,
     emp_role: req.body.emp_role,
     emp_sap_id: req.body.emp_sap_id,
     emp_att_id: req.body.emp_att_id,
     project_sr_no: req.body.project_sr_no,
     emp_skills: req.body.emp_skills,
     bill_rate: req.body.bill_rate,
     
  }},
  options = {new: true};

  Employee.findOneAndUpdate(query, updateData, options, (err, result)=>{
    if(err) {
      console.error('error in updateEntity ' + err);
      res.json({"error":err})
    } else {
      console.log('data updated in project-- ' + result)
      res.json(result)
    }
  })

})

// @route   DELETE api/employees/:_id
// @desc    Delete employees
// @access  Private
router.delete(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const id = req.params._id
    Employee.remove({ _id: id })
     .exec()
     .then(result => {
       req.flash('success_msg', 'Emplyee Deleted');
       res.status(200).json(result);
       
     })
     .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});


// @route POST api/profile/projects
//@desc   Add projects to profile
//@access Private

router.post('/', passport.authenticate('jwt', { session: false}), (req,res) =>{

  const { errors, isValid } = validateEmployeeInput(req.body);
    if(!isValid){
      //Return any error with 400
      return res.status(400).json(errors);
    }
    const newEmployee = new Employee({
         
        emp_name: req.body.emp_name,
        emp_sap_id: req.body.emp_sap_id,
        emp_att_id: req.body.emp_att_id,
        emp_role: req.body.emp_role,
        emp_level: req.body.emp_level,
        emp_skills: req.body.emp_skills,
        bill_rate: req.body.bill_rate,
        emp_location: req.body.emp_location

    
    });

    Employee.findOne({ user: req.user.id })
    .then(employee => {
      if(employee) {
        //Update 
        Employee.findOneAndUpdate(
          { user: req.user.id },
          { $set: newEmployee },
          { new: true }
        )
        .then(employee => res.json(employee));
      }else{
        //Create

        //Check if att id exists
        Employee.findOne({ emp_att_id: newEmployee.emp_att_id }).then(employee => {
          if (employee) {
            errors.emp_att_id = 'That Employee ATT id already exists';
            res.status(400).json(errors);
          }

          // Save Project
          
            new Employee(newEmployee).save().then(employee => res.json(employee));
         
         
        });
      }
    });
  }
);



 

module.exports = router;
