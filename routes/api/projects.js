const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

const Project = require('../../models/Project');
const Profile = require('../../models/Profile');
const User = require('../../models/User');

const validateProjectInput = require('../../validation/projects');


// @route   GET api/posts/test
// @desc    Tests post route
// @access  Public
router.get('/all', (req, res) => {
  const errors = {};

  Project.find()
    .populate('user', ['pmt_id', 'project_pid'])
    .then(projects => {
      if (!projects) {
        errors.noproject = 'There are no projects';
        return res.status(404).json(errors);
      }

      res.json(projects);
    })
    .catch(err => res.status(404).json({ project: 'There are no projects' }));
});

// @route GET api/projects/pmt_id
//@desc   GET projects by pmt_id
//@access Public
router.get('/pmt_id/:pmt_id', (req, res) => {
  const errors = {};

  Project.findOne({ pmt_id: req.params.pmt_id })
    .populate('user', ['pmt_id', 'sap_id'])
    .then(projects => {
      if (!projects) {
        errors.noproject = 'There is no projects by this PMT ID';
        res.status(404).json(errors);
      }

      res.json(projects);
    })
    .catch(err => res.status(404).json(err));
});

// PUT projects/:id
// update projects details
// private access

router.put('/:_id', passport.authenticate('jwt', { session: false}),(req, res)=>{
  var query = {"_id":req.params._id},
  updateData = {"$set":{
    project_title: req.body.project_title,
     pmt_id: req.body.pmt_id,
     project_att_manager: req.body.project_att_manager,
     project_att_manager_id: req.body.project_att_manager_id,
     project_cr_no: req.body.project_cr_no,
     project_sr_no: req.body.project_sr_no,
     project_program: req.body.project_program,
     const_non_const: req.body.const_non_const,
     start_date: req.body.start_date,
     end_date: req.body.end_date,
     m_5: req.body.m_5,
     m_7: req.body.m_7,
     m_9: req.body.m_9,
     warranty: req.body.warranty,
     project_project_type: req.body.project_project_type 
  }},
  options = {new: true};

  Project.findOneAndUpdate(query, updateData, options, (err, result)=>{
    if(err) {
      console.error('error in updateEntity ' + err);
      res.json({"error":err})
    } else {
      console.log('data updated in project-- ' + result)
      res.json(result)
    }
  })

})

// @route   DELETE api/projects/:_id
// @desc    Delete projects
// @access  Private
router.delete(
  '/:_id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const id = req.params._id
    Project.remove({ _id: id })
     .exec()
     .then(result => {
       result._id = id;
       res.status(200).json(result);
     })
     .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

// @route   POST api/Projects/Resource Loading
// @desc    Add Resource Loading to Projects
// @access  Private
router.post(
  '/resourceloading',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {

    Project.findOne({ user: req.user.id }).then(projects => {
      const newResource = {
     
     proj_empname : req.body.proj_empname,  
     proj_emp_att_id: req.body.proj_emp_att_id,
     proj_emp_sap_id: req.body.proj_emp_sap_id,   
     proj_emp_bill_rate: req.body.proj_emp_bill_rate,
     proj_emprole: req.body.proj_emprole
     
        
      };
     

      // Add to exp array
      projects.resourceloading.unshift(newResource);
     

      projects.save().then(projects => res.json(projects));
    })
    
  
  });


// @route POST api/profile/projects
//@desc   Add projects to profile
//@access Private

router.post('/', passport.authenticate('jwt', { session: false}), (req,res) =>{

  const { errors, isValid } = validateProjectInput(req.body);
    if(!isValid){
      //Return any error with 400
      return res.status(400).json(errors);
    }
    const newProject = new Project({
    
     project_title: req.body.project_title,
     pmt_id: req.body.pmt_id,
     project_pid: req.body.project_pid,
     project_att_manager: req.body.project_att_manager,
     project_att_manager_id: req.body.project_att_manager_id,
     project_cr_no: req.body.project_cr_no,
     project_sr_no: req.body.project_sr_no,
     project_program: req.body.project_program,
     const_non_const: req.body.const_non_const,
     start_date: req.body.start_date,
     end_date: req.body.end_date,
     m_5: req.body.m_5,
     m_7: req.body.m_7,
     m_9: req.body.m_9,
     warranty: req.body.warranty,
     project_project_type: req.body.project_project_type   

    
    });

    Project.findOne({ user: req.user.id })
    .then(projects => {
      if(projects) {
        //Update 
        Project.findOneAndUpdate(
          { user: req.user.id },
          { $set: newProject },
          { new: true }
        )
        .then(projects => res.json(projects));
      }else{
        //Create

        //Check if att id exists
        Project.findOne({ pmt_id: newProject.pmt_id }).then(projects => {
          if (projects) {
            errors.pmt_id = 'That pmt id already exists';
            res.status(400).json(errors);
          }

          // Save Project
          
            new Project(newProject).save().then(projects => res.json(projects));
          
         
        });
      }
    });
  }
);



 

module.exports = router;
