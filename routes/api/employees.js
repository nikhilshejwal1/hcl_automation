const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

const Project = require('../../models/Project');
const Profile = require('../../models/Profile');
const User = require('../../models/User');
const Employee = require('../../models/Employees');

const validateEmployeeInput = require('../../validation/employee');


// @route   GET api/posts/test
// @desc    Tests post route
// @access  Public
router.get('/all', (req, res) => {
  const errors = {};

  Employee.find()
    .populate('user', ['emp_name', 'emp_att_id'])
    .then(employee => {
      if (!employee) {
        errors.noemployee = 'There are no Employee';
        return res.status(404).json(errors);
      }

      res.json(employee);
    })
    .catch(err => res.status(404).json({ employee: 'There are no Employees' }));
});





// @route POST api/profile/projects
//@desc   Add projects to profile
//@access Private

router.post('/', passport.authenticate('jwt', { session: false}), (req,res) =>{

  const { errors, isValid } = validateEmployeeInput(req.body);
    if(!isValid){
      //Return any error with 400
      return res.status(400).json(errors);
    }
    const newEmployee = {};
     newEmployee.employee = req.employee.att_id; 
     if (req.body.emp_name) profileFields.emp_name = req.body.emp_name;  
     if (req.body.emp_sap_id) profileFields.emp_sap_id = req.body.emp_sap_id; 
     if (req.body.emp_location) profileFields.emp_location = req.body.emp_location;
     if (req.body.emp_role) profileFields.emp_role = req.body.emp_role;
     if (req.body.emp_att_id) profileFields.emp_att_id = req.body.emp_att_id;
     if (req.body.emp_skills) profileFields.emp_skills = req.body.emp_skills;
     if (req.body.bill_rate) profileFields.bill_rate = req.body.bill_rate;
        
    
    

    Employee.findOne({ user: req.user.id })
    .then(employee => {
      if(employee) {
        //Update 
        Employee.findOneAndUpdate(
          { _id: req.params.id },
          { $set: newEmployee },
          { new: true }
        )
        .then(employee => res.json(employee));
      }else{
        //Create

        //Check if att id exists
        Employee.findOne({ emp_att_id: newEmployee.emp_att_id }).then(employee => {
          if (employee) {
            errors.emp_att_id = 'That Employee ATT id already exists';
            res.status(400).json(errors);
          }

          // Save Project
          
            new Employee(newEmployee).save().then(employee => res.json(employee));
         
         
        });
      }
    });
  }
);



 

module.exports = router;
