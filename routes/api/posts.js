const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

const Projects = require('../../models/Project');
const Profile = require('../../models/Profile');
const User = require('../../models/User');

// @route   GET api/posts/test
// @desc    Tests post route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Posts Works' }));

// @route POST api/profile/projects
//@desc   Add projects to profile
//@access Private

router.post('/projects', passport.authenticate('jwt', { session: false}), (req,res) =>{

  const { errors, isValid } = validateProjectsInput(req.body);
    if(!isValid){
      //Return any error with 400
      return res.status(400).json(errors);
    }

    const newProject = {}
      newProject.user = req.user.id;
      
      if (req.body.project_title) newProject.project_title = req.body.project_title;

      if (req.body.pmt_id) newProject.pmt_id = req.body.pmt_id;
      if (req.body.project_pid) newProject.project_pid = req.body.project_pid;
      if (req.body.project_att_manager) newProject.project_att_manager = req.body.project_att_manager;
      if (req.body.project_att_manager_id) newProject.project_att_manager_id = req.body.project_att_manager_id;
      if (req.body.project_cr) newProject.project_cr = req.body.project_cr;
      if (req.body.project_program) newProject.project_program = req.body.project_program;
      if (req.body.project_sr) newProject.project_sr = req.body.project_sr;
      
      if (req.body.project_project_type) newProject.project_project_type = req.body.project_project_type;    

    
      Profile.findOne({ user: req.user.id })
      .then(profile => {
        if(profile) {
          //Update 
          Profile.findOneAndUpdate(
            { user: req.user.id },
            { $set: profileFields },
            { new: true }
          )
          .then(profile => res.json(profile));
        }else{
          //Create
  
  
          //Check if att id exists
          Profile.findOne({ att_id: profileFields.att_id }).then(profile => {
            if (profile) {
              errors.att_id = 'That att id already exists';
              res.status(400).json(errors);
            }
  
            // Save Profile
            new Profile(profileFields).save().then(profile => res.json(profile));
          });
        }
      });
    }
  );
    
 

  



module.exports = router;
