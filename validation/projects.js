const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateProjectInput(data) {
  let errors = {};

  data.pmt_id = !isEmpty(data.pmt_id) ? data.pmt_id : '';

  data.project_pid = !isEmpty(data.project_pid) ? data.project_pid : '';
   
  data.project_title= !isEmpty(data.project_title) ? data.project_title: '';
  data.project_att_manager= !isEmpty(data.project_att_manager) ? data.project_att_manager: '';

  data.project_att_manager_id= !isEmpty(data.project_att_manager_id) ? data.project_att_manager_id: '';

  data.project_cr_no= !isEmpty(data.project_cr_no) ? data.project_cr_no: '';

  data.project_sr_no= !isEmpty(data.project_sr_no) ? data.project_sr_no: '';
  data.project_program= !isEmpty(data.project_program) ? data.project_program: '';

  data.project_project_type= !isEmpty(data.project_project_type) ? data.project_project_type: '';

  data.const_non_const= !isEmpty(data.const_non_const) ? data.const_non_const: '';

  
  if (Validator.isEmpty(data.pmt_id)) {
    errors.pmt_id = 'Project PMT ID is required';
  }
  if (Validator.isEmpty(data.project_pid)) {
    errors.project_pid = 'Project ID is required';
  }
  if (Validator.isEmpty(data.project_title)) {
    errors.project_title = 'Project title is required';
  }
  if (Validator.isEmpty(data.project_att_manager)) {
    errors.project_att_manager = 'Project ATT Manager is required';
  }
  if (Validator.isEmpty(data.project_att_manager_id)) {
    errors.project_att_manager_id = 'Project ATT Manager ID is required';
  }
  if (Validator.isEmpty(data.project_cr_no)) {
    errors.project_cr_no = 'Project CR No. is required';
  }
  if (Validator.isEmpty(data.project_sr_no)) {
    errors.project_sr_no = 'Project SR No. is required';
  }
  if (Validator.isEmpty(data.project_project_type)) {
    errors.project_project_type = 'Project Type is required';
  }
  if (Validator.isEmpty(data.project_program)) {
    errors.project_program = 'Project Program is required';
  }
  if (Validator.isEmpty(data.const_non_const)) {
    errors.const_non_const = 'Construction or Non-Constuctionis required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
