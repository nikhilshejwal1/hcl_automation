const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateLoginInput(data) {
  let errors = {};

  data.att_id = !isEmpty(data.att_id) ? data.att_id : '';
  data.password = !isEmpty(data.password) ? data.password : '';
 

  if (!Validator.isEmail(data.att_id)) {
    errors.att_id = 'ATT ID is invalid';
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = 'Password field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
