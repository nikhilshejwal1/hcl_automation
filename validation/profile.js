const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateProfileInput(data) {
  let errors = {};

  data.sap_id = !isEmpty(data.sap_id) ? data.sap_id: '';
  data.att_id = !isEmpty(data.att_id) ? data.att_id: '';
  data.emp_role = !isEmpty(data.emp_role) ? data.emp_role: '';
  data.emp_location = !isEmpty(data.emp_location) ? data.emp_location: '';
  data.projects_assigned = !isEmpty(data.projects_assigned) ? data.projects_assigned: '';

  if (Validator.isEmpty(data.sap_id)) {
    errors.sap_id = 'SAP ID field is required';
  }
  if (Validator.isEmpty(data.att_id)) {
    errors.att_id = 'ATT ID is required';
  }
  if (Validator.isEmpty(data.projects_assigned)) {
    errors.projects_assigned = 'Project is required';
  }
  if (Validator.isEmpty(data.emp_location)) {
    errors.emp_location = 'Location is required';
  }
  if (Validator.isEmpty(data.emp_role)) {
    errors.emp_role = 'Employee role is required';
  }
  

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
