const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.att_id = !isEmpty(data.att_id) ? data.att_id : '';
  data.sap_id = !isEmpty(data.sap_id) ? data.sap_id : '';
  data.emp_role = !isEmpty(data.emp_role) ? data.emp_role : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.password2 = !isEmpty(data.password2) ? data.password2 : '';

  if (!Validator.isLength(data.name, { min: 2, max: 30 })) {
    errors.name = 'Name must be between 2 and 30 characters';
  }

  //if (!Validator.isAlphanumeric(data.att_id, 'en-US' )) {
  //  errors.att_id = 'ATT ID should be Alphanumeric only for example- //ab123c';
  //}
  
  if (Validator.isEmpty(data.name)) {
    errors.name = 'Name field is required';
  }

  if (Validator.isEmpty(data.att_id)) {
    errors.att_id = 'ATT ID field is required';
  }

  if (Validator.isEmpty(data.sap_id)) {
    errors.sap_id = 'SAP ID field is required';
  }
  if (Validator.isEmpty(data.emp_role)) {
    errors.emp_role = 'Employee Role field is required';
  }



  if (Validator.isEmpty(data.password)) {
    errors.password = 'Password field is required';
  }

  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = 'Password must be at least 6 characters';
  }

  if (Validator.isEmpty(data.password2)) {
    errors.password2 = 'Confirm Password field is required';
  }

  if (!Validator.equals(data.password, data.password2)) {
    errors.password2 = 'Passwords must match';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
