const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateEmployeeInput(data) {
  let errors = {};

  data.emp_sap_id = !isEmpty(data.emp_sap_id) ? data.emp_sap_id: '';
  data.emp_att_id = !isEmpty(data.emp_att_id) ? data.emp_att_id: '';
  data.emp_name = !isEmpty(data.emp_name) ? data.emp_name: '';
  data.emp_location = !isEmpty(data.emp_location) ? data.emp_location: '';
  data.emp_role = !isEmpty(data.emp_role) ? data.emp_role: '';
  data.emp_level = !isEmpty(data.emp_level) ? data.emp_level: '';
  data.emp_skills = !isEmpty(data.emp_skills) ? data.emp_skills: '';
  data.bill_rate = !isEmpty(data.bill_rate) ? data.bill_rate: '';
  

  if (Validator.isEmpty(data.emp_sap_id)) {
    errors.emp_sap_id = 'SAP ID field is required';
  }
  if (Validator.isEmpty(data.emp_att_id)) {
    errors.emp_att_id = 'ATT ID is required';
  }
  if (Validator.isEmpty(data.emp_name)) {
    errors.emp_name = 'Employee Name is required';
  }
  if (Validator.isEmpty(data.emp_role)) {
    errors.emp_role = 'Employee Role is required';
  }
  if (Validator.isEmpty(data.emp_location)) {
    errors.emp_location = 'Employee Location is required';
  }
  if (Validator.isEmpty(data.emp_level)) {
    errors.emp_level = 'Employee Level is required';
  }
  if (Validator.isEmpty(data.emp_skills)) {
    errors.emp_skills = 'Employee Skills is required';
  }
  if (Validator.isEmpty(data.bill_rate)) {
    errors.bill_rate = 'Bill Rate is required';
  }
  
  

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
